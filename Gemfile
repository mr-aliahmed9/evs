source 'https://rubygems.org'
ruby '2.3.1'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
group :development, :test do
  gem 'byebug', platform: :mri
end
group :development do
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'bootstrap-sass'
gem 'devise'
gem 'haml-rails'
gem 'high_voltage'
gem 'pg'
gem 'simple_form'

# Customized bootstrap theme
gem 'bootswatch-rails'

# Font Awesome
gem "font-awesome-rails"

# Wizard
gem 'wicked', '~> 1.3', '>= 1.3.0'

# Add a comment summarizing the current schema to the top or bottom of each of your
# - ActiveRecord models
# - Tests and Specs
gem 'annotate', '~> 2.7', '>= 2.7.1'

# Generate ERD diagram of Models Schemas
gem "rails-erd"

# Admin interface
gem 'activeadmin', github: 'activeadmin'
gem 'inherited_resources', github: 'activeadmin/inherited_resources'
gem 'active_skin'

# Braintree Payment Gateway
gem "braintree", "~> 2.68.2"
gem 'braintree-rails', :github => "lyang/braintree-rails", :branch => 'master'

# Send Server variable to JS
gem 'gon', '~> 6.1'

# Mongoid (ORM)
gem 'mongoid', '~> 6.0.0'
gem 'bson', '~> 4.0'

# Address geocoders
gem 'geokit', '~> 1.10'

# Background servers run up tool
gem 'foreman'

# Attachments
gem "paperclip", "~> 5.0.0"

# Client side Validation
gem 'jquery-validation-rails'

# Inline Editing
gem 'best_in_place', '~> 3.0.1'

# PDF
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

# Fake Data Generator
gem 'faker', '~> 1.7', '>= 1.7.2'

# Webserver
gem 'passenger'

gem 'aws-sdk', '~> 2'

gem 'jquery-datatables-rails', '~> 3.4.0'
group :development, :test do
  gem 'railroady'
end
group :development do
  gem 'better_errors'
  gem 'guard-bundler'
  gem 'guard-rails'
  gem 'guard-rspec'
  gem 'html2haml'
  gem 'rails_layout'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false
  gem 'spring-commands-rspec'
  gem 'binding_of_caller'
end
group :development, :test do
  gem 'factory_girl_rails'
  gem 'rspec-rails'
end
group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem "selenium-webdriver", "~> 2.38.0"
  gem "chromedriver-helper"
end
