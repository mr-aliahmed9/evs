EVS - Education Voucher Scheme
================

Prerequisites
-------------

This application requires:

- Ruby 2.3.1
- Rails 5.0.0.1

Getting Started
---------------

## How to run application on SSL:
  * Check the following link https://makandracards.com/makandra/35093-how-to-enable-ssl-in-development-with-passenger-standalone

  ### Install a Self Signed Request
   * openssl req -new -newkey rsa:2048 -sha1 -days 365 -nodes -x509 -keyout server.key -out server.crt
   * Place server.key and server.crt to somewhere in ssl directory
   * Start standalone passenger with
        * passenger start --ssl --ssl-certificate **/server.crt --ssl-certificate-key **/server.key --ssl-port 3001
        * Note we are mentioning ssl port. It's because ssl requires an independent port to listen to the server port which is default 3000
        
   #### With Passenger 
   * passenger: passenger start --ssl --ssl-certificate **/localhost.crt --ssl-certificate-key **/localhost.key --ssl-port 3001
   #### With Puma
   * puma -b 'ssl://127.0.0.1:3000?key=ssl/localhost.key&cert=ssl/localhost.crt'
    
  ### Start with Foreman
   * Install a Gem Foreman
     * gem install foreman
   * Create a Procfile in root of folder if not created and add following
   * Define processes in Procfile like
        * puma: puma -b 'ssl://127.0.0.1:3000?key=ssl/localhost.key&cert=ssl/localhost.crt'
        * passenger: passenger start --ssl --ssl-certificate **/localhost.crt --ssl-certificate-key **/localhost.key --ssl-port 3001
   * Check if Procfile is good with
     * foreman check
   * Run application through with
     * foreman start
   * Or run in specific environment with
     * foreman start -e development.env || foreman run -e development.env rails c
     * It loads environment variables listed in development.env
   * Start application on "https://lvh.me:3000"
   * Or without subdomain on "https://localhost:3000"

