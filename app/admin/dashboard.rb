ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }
  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate", style: "margin-top: 0px;" do
        span "Welcome to your Dashboard"
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "New Users" do
          if User.all.present?
            table_for User.order("created_at desc").limit(5) do
              column :full_name do |user|
                user.full_name
              end
              column :email do |user|
                user.email
              end
              column :username do |user|
                user.username
              end
              column :user_type do |user|
                user.active_participant.try(:capitalize)
              end
              column "" do |user|
                link_to "View Details", admin_user_path(user)
              end
            end
            strong { link_to "View All Users", admin_users_path }
          else
            div class: "blank_slate_container", id: "dashboard_default_message" do
              span class: "blank_slate" do
                span "There are no new users"
              end
            end
          end
        end
      end

      column do
        panel "Requisitions" do
          if Requisition.all.present?
            table_for Requisition.order("created_at desc").limit(5) do
              column :subject
              column :owner
              column :owner_type
              column :status do |resq|
                status_tag 'active', :ok, class: 'approved', label: resq.status.humanize
              end
              column "" do |resq|
                link_to "View Details", admin_requisition_path(resq)
              end
            end
            strong { link_to "View All Requisitions", admin_requisitions_path }
          else
            div class: "blank_slate_container", id: "dashboard_default_message" do
              span class: "blank_slate" do
                span "There are no new requisitions"
              end
            end
          end
        end
      end
    end

    panel "Invoices" do
      if Invoice.all.present?
        table_for Invoice.order("created_at desc").limit(5) do
          column :invoice_number
          column :invoice_amount
          column :month do |i|
            i.month_humanize
          end
          column :year
          column "" do |i|
            link_to "View Detail", admin_invoice_path(i)
          end
        end
        strong { link_to "View All Invoices", admin_invoices_path }
      else
        div class: "blank_slate_container", id: "dashboard_default_message" do
          span class: "blank_slate" do
            span "There are no recent invoices"
          end
        end
      end
    end


  end # content
end
