ActiveAdmin.register Invoice do
  menu parent: "Payments"
  config.clear_action_items!

  actions :index, :show

  index do
    id_column
    column :issued_at
    column :requisition
    column :sponsor do |i|
      i.requisition.try(:provider).try(:full_name)
    end
    column :invoice_amount
    column :status do |i|
      i.status.humanize
    end
    column :invoice_number
    column "Month" do |i|
      i.month_humanize
    end
    column :year
    actions
  end
end
