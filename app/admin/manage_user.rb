ActiveAdmin.register User, as: "Manage Users" do
  menu priority: 2
  config.clear_action_items!

  actions :all, except: [:edit, :destroy]

  scope :approved, :default => true do |users|
    users.with_profile_status(:profile_completed)
  end

  scope :blocked do |users|
    users.with_profile_status(:blocked)
  end

  index do
    selectable_column
    id_column
    column :full_name
    column :email
    column :username
    column :national_id

    column :address do |user|
      user.participant.address.location
    end
    column :city do |user|
      user.participant.address.city
    end

    actions defaults: true do |user|
      if "approved" == params[:scope] or params[:scope].nil?
        link_to 'Block', block_admin_manage_user_path(user), method: :put
      else
        link_to 'Unblock', unblock_admin_manage_user_path(user), method: :put
      end
    end
  end

  member_action :block, method: :put do
    resource.profile_status = User::PROFILE_STATUS[:blocked]
    resource.save(validate: false)
    redirect_to admin_manage_users_path(scope: :approved), notice: "Blocked!"
  end

  member_action :unblock, method: :put do
    resource.profile_status = User::PROFILE_STATUS[:profile_completed]
    resource.save(validate: false)
    # NotificationsMailer.waiting_approval(resource).deliver
    redirect_to admin_manage_users_path(scope: :blocked), notice: "Unblocked!"
  end
end
