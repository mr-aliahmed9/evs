include ActiveAdmin::RequisitionsHelper
ActiveAdmin.register Requisition do
  config.clear_action_items!

  actions :index, :show

  scope :sponsorship, :default => true do |requisitions|
    requisitions.sponsorship
  end

  scope :audit do |requisitions|
    requisitions.audit
  end

  index do
    selectable_column
    id_column
    column :subject
    column :tenure
    column :start_date
    column :end_date
    column :status do |requisition|
    	status_tag 'active', :ok, class: 'approved', label: requisition.status.humanize
    end
    column :school
    actions
    # actions do |resource|
    #   item "View Detail", admin_requisition_path(resource)
    # end
  end

  show do
    panel "Requisition Details" do
      attributes_table_for requisition do
        row :subject
        row :description
        row :tenure
        row :start_date
        row :end_date
        row :status do |requisition|
        	status_tag 'active', :ok, class: 'approved', label: requisition.status.humanize
        end
        row :school
        if requisition.owner_type == "Sponsor"
          row :sponsorship_requisition
          row :audit_offers do |r|
            r.audit_offers.count
          end
        else
          row :audit_requisition
          row :sponsorship_offers do |r|
            r.sponsorship_offers.count
          end
        end
        row :owner
        row :owner_type
        row :provider
        row :provider_type
        row :grade_level
        row :published_at
        row :student
      end
    end
    if requisition.owner_type == "Sponsor"
      panel "Audit Report Progress" do
        attributes_table_for requisition do
          row "Approved On" do |r|
            r.approved_at
          end
          row "Finished On" do |r|
            if r.audit_report.present?
              r.sponsorship_requisition.report_uploaded_at
            else
              "Requisition On Going"
            end
          end
          row "Total Time" do |r|
            if r.audit_report.present?
              r.sponsorship_requisition.audit_completed_in
            else
              "Requisition On Going"
            end
          end
        end
      end
    else

      if requisition.audit_requisition.present? and requisition.audit_requisition.completed?
        panel "Audit Report" do
          table_for requisition.audit_requisition do
            column "Total Marks" do |r|
              number_with_decimal r.audit_report.total_marks
            end
            column "Out Of" do |r|
              r.audit_report.total_report_marks
            end
            column "Percentage (%)" do |r|
              number_to_percentage r.audit_report.percentage
            end
            column "Result" do |r|
              status_tag 'active', :ok, class: 'approved', label: r.audit_report.result.humanize
            end
            column "PDF Report" do |r|
              link_to download_audit_report_admin_requisitions_path(requisition_id: r.id, audit_report_id: r.audit_report, deposition: "attachment", format: :pdf) do
                image_tag "pdf.png", class: "img-responsive", width: "50px"
              end
            end
          end
        end
        panel "Invoices (#{requisition.invoices.count})" do
          table_for requisition.invoices do
            column "Invoice Amount" do |i|
              number_with_decimal i.invoice_amount
            end
            column "Status" do |i|
              i.status.humanize
            end
            column :invoice_number
            column "Month" do |i|
              i.month_humanize
            end
            column :year
          end
        end
      end
    end
  end

  collection_action :download_audit_report, :method => :get do
    @report = AuditReport.find(params[:audit_report_id])
    @requisition = Requisition.find(params[:requisition_id])
    respond_to do |format|
      format.pdf do
        pdf  = render_to_string pdf: "EVS-Audit-Report",
               template: 'requisitions/reports/show.pdf.haml',
               layout: '/layouts/evs_audit_report.html.haml',
               print_media_type: true,
               title: "Requisition Audit Report - #{generated_report_format(@report.updated_at)}"
        send_data(pdf, filename: "EVS-Audit-Report-#{generated_report_format(@report.updated_at)}.pdf",  :type=>"application/pdf", :disposition => params[:deposition] || "attachment")
      end
    end
  end
end
