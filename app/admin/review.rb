ActiveAdmin.register Review do

  config.clear_action_items!

  actions :index, :show

  index do
    id_column
    column :rating
    column :comment
    column :requisition
    column :ratee
    column :rater
    actions
  end
end
