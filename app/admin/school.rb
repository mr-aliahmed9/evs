include ActiveAdmin::SchoolsHelper
ActiveAdmin.register School do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
permit_params :name, :overview, :foundation_date, :staffs, :students, address_attributes: [:id, :location, :latitude, :longitude, :city, :state, :place_id]

  action_item only: :show do
    link_to 'Pay School', "/admin/transactions/new?payee_type=School&payee_id=#{school.id}"
  end

  index do
    selectable_column
    id_column
    column :name
    column :foundation_date do |school|
      founded_date(school.foundation_date)
    end
    column :staffs
    column :students
    column :city do |school|
      school.address.city
    end
    column :state do |school|
      school.address.state
    end
    actions
  end
  controller do
    before_action :set_school, :set_school_location, only: [:show, :edit]

    def new
      @school = School.new
      @school.build_address
    end

    private

    def set_school
      begin
        @school = School.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        redirect_to admin_schools_path, flash: { alert: e.message }
      end
    end

    def set_school_location
      if @school.address.present?
        set_geocode_location!(@school.address)
      end
    end
  end

  show do
    script :src => javascript_path(google_api(places: true)), :type => "text/javascript"
    script :src => javascript_path("admin/schools"), :type => "text/javascript"
    panel "School Details" do
      attributes_table_for school do
        row :name
        row :overview
        row :foundation_date do |school|
          founded_date(school.foundation_date)
        end
        row :staffs
        row :students
        row "Total requisitions" do |school|
          school.requisitions.count
        end
        row :fees do |school|
          number_to_currency school.fees
        end
        row "Funds In Wallet" do |school|
          number_to_currency school.funds
        end
      end
    end

    panel "School Location" do
      div class: "google-maps" do
        div class: "loading-map" do
          '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Loading Map'.html_safe
        end
      end
    end

    if school.requisitions.present?
      panel "Requisitions" do

        school.requisitions.each do |resq|
          attributes_table_for resq do
            row :subject
            row "View" do |r|
              link_to "Details", admin_requisition_path(r.id)
            end
            row "Invoices" do |r|
              panel "Total (#{r.invoices.count})" do
                table_for r.invoices do
                  column "Invoice Amount" do |i|
                    number_with_decimal i.invoice_amount
                  end
                  column "Status" do |i|
                    i.status.humanize
                  end
                  column :invoice_number
                  column "Month" do |i|
                    i.month_humanize
                  end
                  column :year
                end
              end
            end
          end
        end
      end
    end
  end

  form do |f|
    script :src => javascript_path(google_api(places: true)), :type => "text/javascript"
    script :src => javascript_path("admin/schools"), :type => "text/javascript"
    inputs 'Details' do
      input :name
      input :overview, label: "Overview"
      li "Created at #{f.object.created_at}" unless f.object.new_record?
      f.input :foundation_date, as: :datepicker, datepicker_options: { change_year: true, change_month: true }
      input :staffs
      input :students
    end

    f.inputs "Address", for: [:address, f.object.address] do |a|
      a.input :location, input_html: { placeholder: "Search from Google", id: "location_input" }
      a.input :city, input_html: { id: "city_input" }
      a.input :state, input_html: { id: "state_input" }
      a.input :latitude, as: :hidden, input_html: { id: "latitude_input" }
      a.input :longitude, as: :hidden, input_html: { id: "longitude_input" }
      a.input :place_id, as: :hidden, input_html: { id: "place_id_input" }
    end
    panel "School Location" do
      div class: "google-maps" do
        div class: "loading-map" do
          '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Loading Map'.html_safe
        end
      end
    end
    actions
  end
end
