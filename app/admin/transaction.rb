ActiveAdmin.register Transaction do
  menu parent: "Payments"

  permit_params :bank_account, :bank_name, :branch, :check_number, :payorder_number, :amount, :payee_id, :payee_type

  scope :schools, :default => true do |trans|
    trans.school_transactions
  end

  scope :auditors do |trans|
    trans.auditor_transactions
  end

  config.clear_action_items!

  action_item :only => :index do
    unless params[:scope] == "auditors"
      link_to "Create School Transaction" , "/admin/transactions/new?payee_type=School"
    else
      link_to "Create Auditor Transaction" , "/admin/transactions/new?payee_type=Auditor"
    end
  end

  controller do
    def new
      unless params[:payee_type].present?
        redirect_to admin_transactions_path, flash: { alert: "Payee Type must be present" } and return
      end
      @transaction = Transaction.new(payee_type: params[:payee_type], payee_id: params[:payee_id])
      @payees = params[:payee_type].constantize.all
    end
  end

  index do
    column :bank_account
    column :bank_name
    column :branch
    column :check_number
    column :payorder_number
    column :amount
  end

  form url: "/admin/transactions", method: :post do |f|
    inputs 'Bank Details' do
      input :bank_account
      input :bank_name
      input :branch
      input :check_number, label: "Cheque Number"
      input :payorder_number
      # input :amount
    end

    inputs 'Payee Details' do
      div do
        hidden_field_tag :payee_id, params[:payee_id]
      end
      div do
        hidden_field_tag :payee_type, params[:payee_type]
      end
      f.input :payee_id, :as => :select, :collection => (params[:payee_type]).constantize.all.map {|payee| [payee.is_a?(Auditor) ? payee.full_name : payee.name, payee.id] }, selected: (f.object.payee_id || params[:payee_id]), include_blank: false
      f.input :payee_type, label: "Payee Type", input_html: { value: params[:payee_type] }
    end
    actions
  end
end
