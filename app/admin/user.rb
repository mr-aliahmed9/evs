ActiveAdmin.register User do
  menu priority: 3
  config.clear_action_items!

  # Create a action link
  # action_item(:index) do
  #   link_to "New Agent" , "/admin/users/new" if params[:scope] == "agents"
  # end

  action_item only: :show do
    if user.has_role? Auditor
      link_to 'Pay Auditor', "/admin/transactions/new?payee_type=Auditor&payee_id=#{user.participant.id}"
    end
  end

  scope :sponsors, :default => true do |users|
    users.with_role(:sponsor)
  end

  scope :students do |users|
    users.with_role(:student)
  end

  scope :auditors do |users|
    users.with_role(:auditor)
  end

  controller do
    before_action :set_user, :set_school_location, only: [:show]

    private

    def set_user
      begin
        @user = User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        redirect_to admin_users_path, flash: { alert: e.message }
      end
    end

    def set_school_location
      if @user.participant.address.present?
        set_geocode_location!(@user.participant.address)
      end
    end
  end

  index do
    selectable_column
    id_column
    column :full_name
    column :email
    column :username
    column :ntn_number
    actions
  end

  show do
    script :src => javascript_path(google_api(places: true)), :type => "text/javascript"
    script :src => javascript_path("admin/users"), :type => "text/javascript"

    panel "General Details" do
      attributes_table_for user do
        row :full_name
        row :username
        row :location do |user|
          user.participant.address.location
        end
        row :city do |user|
          user.participant.address.city
        end
        row :state do |user|
          user.participant.address.state
        end
      end
    end
    panel "Location in Map" do
      div class: "google-maps" do
        div class: "loading-map" do
          '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Loading Map'.html_safe
        end
      end
    end
    attributes_table_for user do
      user.fetch_all_participants.each do |role|
        panel "#{role.name} Details" do
          attributes_table_for role do
            case role
            when Sponsor
              row :volunteer
              if role.organization
                row :organization
                row :organization_title
              end
              row :overview
              row "Funds In Wallet" do |u|
                number_to_currency u.funds
              end
            when Student
              row :gender
              row :age
              row :date_of_birth
              row :guardian_name
              row :guardian_monthly_income
              row :current_school
              row :bio
            when Auditor
              row :gender
              row :qualification
              row :past_experience
              row :skills
              row :bio
              row "Funds In Wallet" do |u|
                number_to_currency u.funds
              end
            end
          end
        end
      end
    end
  end
end
