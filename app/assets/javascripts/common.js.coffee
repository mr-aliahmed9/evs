#= require jquery.validate
#= require jquery.validate.additional-methods
class @MainSubject
  @observers: []
  @subscribe: (observer) ->
    @observers.push(observer)

  @unsubscribe: (fn) ->
    @observers = @observers.filter((item) ->
      if item == fn
        return item
      return
    )

  @publish: (message, key, value) ->
    @observers.forEach (item) ->
      if Array.isArray(message)
        message.forEach (msg) ->
          item[msg](key, value)
      else
        item[message](key, value)

@Landing =
  initializeParticipantChartGraphs: ->
    chart = new (CanvasJS.Chart)('totalParticipantsChartContainer',
      title: text: 'Participants On Our Site'
      legend:
        maxWidth: 350
        itemWidth: 120
      data: [ {
        type: 'pie'
        showInLegend: true
        legendText: '{indexLabel}'
        dataPoints: [
          {
            y: 500
            indexLabel: 'Sponsors'
          }
          {
            y: 1500
            indexLabel: 'Students'
          }
          {
            y: 800
            indexLabel: 'Auditors'
          }
        ]
      } ])
    chart.render()
    return

  initializeRequisitionsChartGraphs: ->
    chart = new (CanvasJS.Chart)('requisitionsChartContainer',
      title: text: 'Requisitions Applied'
      data: [ {
        type: 'doughnut'
        dataPoints: [
          {
            y: 400
            indexLabel: 'Applied'
          }
          {
            y: 300
            indexLabel: 'On Going'
          }
          {
            y: 150
            indexLabel: 'Validated'
          }
          {
            y: 250
            indexLabel: 'Rejected'
          }
        ]
      } ])
    chart.render()
    return

  initializeRatingChartGraphs: ->
    chart = new (CanvasJS.Chart)('ratingChartContainer',
      title: text: 'Highly Rated Sponsors & Auditors'
      data: [ {
        type: 'doughnut'
        dataPoints: [
          {
            y: 4
            indexLabel: 'Sponsors'
          }
          {
            y: 2.5
            indexLabel: 'Auditors'
          }
        ]
      } ])
    chart.render()
    return

  initializeFundingChart: ->
    chart = new (CanvasJS.Chart)('fundingChartContainer',
      title: text: 'Total Funding 2017'
      data: [
        {
          type: 'bar'
          dataPoints: [
            {
              y: 40000
              label: 'Sponsorship Requisitions'
            }
          ]
        }
        {
          type: 'bar'
          dataPoints: [
            {
              y: 20000
              label: 'Audit Requisitions'
            }
          ]
        }
      ])
    chart.render()
    return

class @Settings
  @loadSpinner: false
  @current_location: []

  initializeLocation: () =>
    # Settings.current_location = gon.location
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition(@showPosition)
    else
      alert 'Geolocation is not supported by this browser.'
    return

  showPosition: (position) =>
    if typeof gon != "undefined" && gon.location != "undefined"
      Settings.current_location = gon.location
    else
      console.log position
      Settings.current_location.push {}
      Settings.current_location[0].latitude = position.coords.latitude
      Settings.current_location[0].longitude = position.coords.longitude

    Settings.current_location.forEach (location) ->
      console.log 'Latitude: ' + location.latitude + '<br>Longitude: ' + location.longitude
    return

  updateCurrentLocation: (key, value) =>
    Settings.current_location.forEach (location) ->
      location[key] = value

  updateLocationInputs: (key, value) =>
    @inputs[key].value = value

  @clear_turbolinks_cache: =>
    document.addEventListener 'turbolinks:before-visit', ->
      if $('#external_javascript').length
        $('#external_javascript').remove()
      return

  @initializeDataTables: (elem) =>
    $(elem).DataTable()

  @notifier: (options = {}) =>
    $.notify {
      icon: "glyphicon glyphicon-warning-sign "
      title: ' Alert <br />'
      message: options.message
    },
      element: 'body'
      position: null
      type: options.type
      offset: 20
      spacing: 10
      z_index: 10000
      delay: 5000
      timer: 1000
      animate:
        enter: 'animated bounceInRight'
        exit: 'animated bounceOutRight'
    return false

  @holdForm: (flag) ->
    $("form").find("input[type='submit']").prop("disabled", flag)

  @modalPopUp: ->
    if $('.modal-popup').data("valid") == true and $('.modal-popup').length
      $('.modal-popup').find(".modal").modal
        show: true
        backdrop: 'static'
        keyboard: $('.modal-popup').data("keyboard") || true

class @Location extends Settings
  constructor: (location = {}, mapOnly = false) ->
    @location_param = location
    if mapOnly == false
      @inputs =
        location: document.getElementById("location_input")
        latitude: document.getElementById("latitude_input")
        longitude: document.getElementById("longitude_input")
        place_id: document.getElementById("place_id_input")
        city: document.getElementById("city_input")
        state: document.getElementById("state_input")

  startMap: (callback) =>
    # Checking if page has spinner when first load
    # Map needs time to load, since body is hidden on first load
    if Settings.loadSpinner == false
      timeOut = 2000
    else
      timeOut = 0

    setTimeout(->
      if !jQuery.isEmptyObject @location_param
        Settings.current_location[0] = @location_param
      else if typeof gon != "undefined" && gon.location != "undefined"
        Settings.current_location = gon.location


      mapInterval = setInterval(->
        if Settings.current_location.length != 0
          clearInterval mapInterval
          callback()
      , 100)
    , timeOut)

  setMap: =>
    that = this
    containers = document.getElementsByClassName("google-maps")
    that.startMap ->
      Settings.current_location.forEach (location, index) ->
        GoogleMaps.initMap containers[index], location

  setMapForm: ->
    that = this
    inputs = that.inputs
    @startMap ->
      container = document.getElementsByClassName("google-maps")[0]
      googleMap = new GoogleMaps

      MainSubject.subscribe(that)

      map = GoogleMaps.initMap container, Settings.current_location[0], (marker) ->
        MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "latitude", marker.latLng.lat())
        MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "longitude", marker.latLng.lng())

        if marker.placeId
          googleMap.getPlaceDetails map, marker.placeId, (place) ->
            console.log "Place", place
            MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "location", place.name)
            MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "place_id", marker.placeId)

      if that.inputs.location
        googleMap.initAutocompleteWithMap map, inputs.location, (place) ->
          console.log "Autocomplete Place", place

          if inputs.city
            inputs.city.value = place.address_components[2].short_name
          if inputs.state
            inputs.state.value = place.address_components[4].short_name
          MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "latitude", place.geometry.location.lat())
          MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "longitude", place.geometry.location.lng())
          MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "place_id", place.place_id)

      MainSubject.unsubscribe(that)

  setAutoCompleteSearch: ->
    MainSubject.subscribe(this)
    input = document.getElementById("location_input")

    GoogleMaps.initAutocomplete null, input, (place) ->
      MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "latitude", place.geometry.location.lat())
      MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "longitude", place.geometry.location.lng())
      MainSubject.publish(["updateCurrentLocation", "updateLocationInputs"], "place_id", place.place_id)

    MainSubject.unsubscribe(this)

class window.Utilities
  @formatCurrency = (total, currency = "$", opt = {}) ->
    neg = false
    total = 0 if total == ""
    if total < 0
      neg = true
      total = Math.abs(total)

    amount = parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').toString()
    currency = (if neg then " -#{currency} " else " #{currency} ")
    if opt.format and opt.format == "%n"
      amount + currency
    else
      currency + amount

# Page Specific Javascripts Bindings
# E.g =>
# $(document).bind 'edit_users.load', (e,obj) =>
#   # fire on edit users controller action
#
# $(document).bind 'show_users.load', (e,obj) =>
#   # fire on show users controller action
#
# $(document).bind 'users.load', (e,obj) =>
#   # fire on all users controller actions

load_generic_javascript = (controller, action) ->
  $.event.trigger "#{controller}.load"
  $.event.trigger "#{action}_#{controller}.load"

# Starting Spinner on first page load
$(window).on 'load', (e) ->
  if typeof gon != 'undefined' && gon.post_params && gon.post_params.method == "POST"
    $("body").find("spinner").hide()
    $("body").find("yield").show()
  else
    $("body").find("spinner").show()
    setTimeout(->
      $("body").find("spinner").hide()
      $("body").find("yield").show()
    , 1500)

# Hiding Spinner when page change after loaded
$(document).on "turbolinks:before-render", (e) ->
  Settings.loadSpinner = true
$(document).on "turbolinks:render", (e) ->
  $("body").find("spinner").hide()
  $("body").find("yield").show()

$(document).on "turbolinks:load", (e) ->
  # Hide Flash Message After 5 Seconds
  window.setTimeout (->
    $('.flash-messages').fadeTo(500, 0).slideUp 500, ->
      $(this).hide()
  ), 5000

  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()

  $(document).on "click", ".dismissable-parent", (e) ->
    e.preventDefault()
    $(this).parent().remove()

  $(".review-rating").rating({
    showClear: false
    showCaption: false
    displayOnly: true
  })

#   # Placing generic javascript
#   load_generic_javascript($("body").data('controller'),$("body").data('action'))
