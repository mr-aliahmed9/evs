#= require jquery
#= require turbolinks
#= require jquery_ujs
#= require best_in_place

#= require jquery-ui
#= require best_in_place.jquery-ui
#= require bootstrap-sprockets

#= require typeahead
#= require datepicker.min
#= require bootbox.min
#= require notify.min
#= require summernote.min
#= require jquery-tmpl.min
#= require star-rating.min
#= require dataTables/jquery.dataTables
#= require dataTables/bootstrap/3/jquery.dataTables.bootstrap

#= require canvasjs.min
#= require jquery.canvasjs.min

#= require_tree ./dashboard
#= require common
#= require_tree ./dashboard/profiles


#Override the default confirm dialog by rails
$.rails.allowAction = (link) ->
  if $(".executeable").length or link.data('confirm') == undefined
    return true
  $.rails.showConfirmationDialog link
  false

#User click confirm button
$.rails.confirmed = (link) ->
  link.addClass "executeable"
  link.trigger 'click.rails'
  return

#Display the confirmation dialog
$.rails.showConfirmationDialog = (link) ->
  message = link.data('confirm')
  title = link.data('confirm-title')
  $link = link
  box = bootbox.confirm
    title: title
    message: message || "Hello"
    buttons:
      confirm:
        label: '<i class="fa fa-check"></i> Yes'
        className: 'btn-success'
      cancel:
        label: '<i class="fa fa-times"></i> No'
        className: 'btn-danger'
    callback: (result) ->
      if result == true
        $.rails.confirmed $link
        box.modal("hide")
        return
  return
