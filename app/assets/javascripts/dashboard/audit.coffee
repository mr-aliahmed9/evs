@AuditReport =
  # This is now disabled since auditor will submit report directly to sponsorship requisition #

  # clearReportUploadForm: ->
  #   form = $(".upload-report-form form")
  #   form.find('input:file').val ''
  #   form.find('.file-name').val ''
  #   form.find('.file-name').hide()
  # browseReport: ->
  #   $("a.upload-report-link").click (e)->
  #     e.preventDefault()
  #     elem = $(@)
  #     $("#upload-report-file-field").click()
  # uploadReport: ->
  #   that = this
  #   $("#upload-report-file-field").on "change", ->
  #     elem = $(@)
  #     form = elem.closest("form")
  #
  #     file = @files[0]
  #     reader = new FileReader
  #
  #     # Set preview image into the popover data-content
  #     reader.onload = (e) ->
  #       bootbox.confirm
  #         title: "Upload Requisition Report?"
  #         message: "Once The report is uploaded. Your Payment will be transfered in to your Wallet."
  #         buttons:
  #           confirm:
  #             label: '<i class="fa fa-check"></i> Upload'
  #             className: 'btn-success'
  #           cancel:
  #             label: '<i class="fa fa-times"></i> Not Now'
  #             className: 'btn-danger'
  #         callback: (result) ->
  #           if result == true
  #             form.find("p.file-name").text file.name
  #             form.find("p.file-name").show().removeClass("hide-content")
  #             form.submit()
  #           else
  #             that.clearReportUploadForm()
  #
  #           console.log 'This was logged in the callback: ' + result
  #           return
  #
  #     reader.readAsDataURL file

  progressReportSubmit: (form, file) ->
    $("#submitReport").on "click", (e) ->
      e.preventDefault()
      elem = $(@)
      bootbox.confirm
        title: "Submit Requisition Report?"
        message: "Once The report is uploaded. Your Payment will be transfered in to your Wallet."
        buttons:
          confirm:
            label: '<i class="fa fa-check"></i> Upload'
            className: 'btn-success'
          cancel:
            label: '<i class="fa fa-times"></i> Not Now'
            className: 'btn-danger'
        callback: (result) ->
          if result == true
            $('#report-files').append $('#reportUploadProgressTemplate').tmpl()
            $('#reportUploadError').addClass 'hide-content'
            elem.addClass("disabled-link")

            $.ajax {
              url: elem.attr('href')
              type: "put"
              # xhr: ->
                # xhr = $.ajaxSettings.xhr()
                # if xhr.upload
                #   xhr.upload.addEventListener 'progress', ((evt) ->
                #     percent = evt.loaded / evt.total * 100
                #     $('#report-files').find('.progress-bar').width percent + '%'
                #     return
                #   ), false
                # xhr
              beforeSend: ->
                progress = 0
                progressInterval = setInterval(->
                  progress++
                  $('#report-files').find('.progress-bar').width progress + '%'
                  if progress == 99
                    clearInterval(progressInterval)
                , 10)

              success: (data) ->
                if JSON.parse(data).status == 200
                  $('#report-files').children().last().remove()
                  $('#report-files').html $('#reportUploadItemTemplate').tmpl(data)
                  setTimeout(->
                    @location.href = JSON.parse(data).redirect
                  , 1500)
                else
                  elem.removeClass("disabled-link")
                  $('#report-files').children().last().remove()
                  $('#reportUploadError').removeClass('hide-content').text "There was a problem while uploading reporting. Please try again!"
                return
              error: ->
                $('#report-files').children().last().remove()
                elem.removeClass("disabled-link")
                $('#reportUploadError').removeClass('hide-content').text "There was a problem while uploading reporting. Please try again!"
                return
              cache: false
              contentType: false
              processData: false
              mimeType: 'multipart/form-data'
            }, 'json'

  initialize: ->
    $('#audit_report_good_controls').summernote({
      height: 250,
      minHeight: null,
      maxHeight: null,
      focus: true
    });
    $('#audit_report_weak_controls').summernote({
      height: 250,
      minHeight: null,
      maxHeight: null,
      focus: true
    });

    $("#audit-items-list li a").click (e) ->
      e.preventDefault()
      $(this).closest("li").toggleClass "audit-item-selected"
      container = $(this).data("target")
      $("#audit-items-fields div##{container}").toggleClass "hide-content"
      container_selected_hidden_field = $("#audit-items-fields div##{container}").find(".selected-audit-item")
      container_destroy_hidden_field = $("#audit-items-fields div##{container}").find(".destroy-audit-item")
      flag_selected_value = container_selected_hidden_field.val()
      container_selected_hidden_field.val(if flag_selected_value == "2" then 1 else "2")

      if container_destroy_hidden_field.length
        flag_destroy_value = container_destroy_hidden_field.val()
        container_destroy_hidden_field.val(if flag_destroy_value == "false" then "true" else "false")
