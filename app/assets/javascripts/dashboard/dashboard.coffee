@Sponsors =
  # Drop UI
  paymentForm: ->
    if typeof gon != 'undefined' && typeof gon.client_token != 'undefined' && $("#braintreeDropin").length
      # Gon is a server variable generated
      braintree.setup gon.client_token, 'dropin',
        container: "braintreeDropin"
        onPaymentMethodReceived: (obj) ->
          # Do some logic in here.
          # When you're ready to submit the form:
          $(".wallet-form #payment_method_nonce").val(obj.nonce)
          $(".wallet-form button.cancel").prop("disabled", true)
          $(".wallet-form .btn-payment").html("<i class='fa fa-spinner fa-pulse'></i> Saving Details")
          setTimeout(->
            $(".wallet-form").submit()
          , 1000)
        onError: (obj ) ->
          if (obj.type == 'VALIDATION')
            console.log obj.details.invalidFields
            setTimeout(->
              $(".wallet-form button.cancel").removeAttr("disabled")
              $(".wallet-form .btn-payment").removeAttr("disabled")
              $(".wallet-form .btn-payment").html("Save Now")
            , 1000)
    return

  initializePayments: ->
    @paymentForm()
    $("#sponsorWalletModalLink").click (e) ->
      e.preventDefault()
      $("#sponsorWalletModal").modal("show")

    $(".wallet-input input[type='number']").on "keyup", ->
      totalFunds = $(".wallet-blockquote").data("funds")
      totalFunds += parseInt($(this).val()) unless $(this).val() == ""
      $(".wallet-blockquote").text(Utilities.formatCurrency(totalFunds, "Rs", { format: "%n" }))
