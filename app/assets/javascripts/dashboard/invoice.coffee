@Invoice =
  submitInvoice: ->
    $("#pay-invoice-submit").on "click", (e) ->
      e.preventDefault()
      elem = $(@)
      if $("input[type='checkbox'][name='invoice_ids[]']:checked").length
        bootbox.confirm
          title: "Pay Invoice"
          message: "Are you sure you want to Pay selected invoices?"
          buttons:
            confirm:
              label: '<i class="fa fa-check"></i> Yes Pay Now'
              className: 'btn-success'
            cancel:
              label: '<i class="fa fa-times"></i> Not Now'
              className: 'btn-danger'
          callback: (result) ->
            if result == true
              elem.closest("form").submit()
      else
        Settings.notifier({message: "Please select Invoice to continue", type: "warning"})
