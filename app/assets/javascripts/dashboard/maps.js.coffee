class @GoogleMaps
  constructor: ->
    @markers = []
    @ready = undefined

  @initializedMap: undefined
  @markerInfoWindow: []

  @initMap: (mapContainer, opt = {}, callback) =>
    markerOpt = {}
    if opt.marker
      markerOpt = opt.marker

    location =
      lat: parseFloat(opt.latitude)
      lng: parseFloat(opt.longitude)

    map = new (google.maps.Map)(mapContainer,
      center: location
      zoom: markerOpt.zoom || 15)

    @initializedMap = map

    self = new GoogleMaps

    if !jQuery.isEmptyObject(markerOpt)
      self.placeMarkers(map, markerOpt, location)

    if jQuery.isEmptyObject(markerOpt) or (!jQuery.isEmptyObject(markerOpt) and markerOpt.click_able_marker)
      google.maps.event.addListener map, 'click', (e) ->
        location.lat = e.latLng.lat()
        location.lng = e.latLng.lng()
        self.setMarkersMap(null) # clear all markers
        console.log e
        self.placeMarkers(map, markerOpt, location)
        if typeof callback == "function"
          callback(e)

    map

  updateMap: (map, location, opt = {}, callback) ->
    marker = new (google.maps.Marker)(
      map: map
      anchorPoint: new (google.maps.Point)(0, -29))

    mapLocation =
      lat: parseFloat(location.latitude)
      lng: parseFloat(location.longitude)

    map.setCenter mapLocation
    map.setZoom opt.zoom || 17

    marker.setPosition mapLocation
    marker.setVisible true
    console.log opt
    if opt.infoWindow
      marker.addListener('click', ->
        infowindow = new (google.maps.InfoWindow)
        content = opt.infoWindow.content
        unless content
          content = ['<div><strong>Set Content</strong><br>'].join("")
        infowindow.setContent content
        infowindow.open map, marker
        if typeof callback == "function"
          callback(infowindow)
      )


  # Places markers and set info window on marker click
  placeMarkers: (map, markerOpt, location) =>
    title = markerOpt.title || ""
    marker = new (google.maps.Marker)(
      position: location
      title: title
      animation: google.maps.Animation.DROP)

    marker.setMap(map)

    iWin = new (google.maps.InfoWindow)(
      content: '<div><strong>' + title + '</strong>'
      title: title
    )

    GoogleMaps.markerInfoWindow.push iWin

    marker.addListener('click', ->
      $.grep(GoogleMaps.markerInfoWindow, (v) ->
        if v.title == marker.title
          v.open(map, marker)
      )
    )

    @markers.push(marker)

    marker

  # Set (add/remove) marker from map
  # map = null  -> remove that marker from map
  # map = value -> add the marker on map based on lat/lng
  setMarkersMap: (map) =>
    i = 0
    while i < @markers.length
      @markers[i].setMap(map)
      i++

  getPlaceDetails: (map, place_id, callback) =>
    result = {}
    service = new google.maps.places.PlacesService(map);
    service.getDetails { placeId: place_id }, (place, status) ->
      if status == google.maps.places.PlacesServiceStatus.OK
        callback place

  getGeocodeLocation: (lat, lng, callback) =>
    latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode { 'latLng': latlng }, (results, status) ->
      result = results[0]
      state = ''
      i = 0
      len = result.address_components.length
      while i < len
        ac = result.address_components[i]
        if ac.types.indexOf('administrative_area_level_1') >= 0
          state = ac
        i++
      callback state
      return

  @holdFormOnAutocomplete: (input) =>
    $(input).focus ->
      $("form").find("input[type='submit'], button[type='submit']").prop("disabled", true)
    $(input).blur ->
      $("form").find("input[type='submit'], button[type='submit']").prop("disabled", false)

  @initAutocomplete: (map, input, callback) =>
    @holdFormOnAutocomplete(input)
    autocomplete = new (google.maps.places.Autocomplete)(input)
    marker = undefined

    if map != null
      autocomplete.bindTo 'bounds', map
      infowindow = new (google.maps.InfoWindow)

      marker = new (google.maps.Marker)(
        map: map
        anchorPoint: new (google.maps.Point)(0, -29))

    autocomplete.addListener 'place_changed', ->
      place = autocomplete.getPlace()
      callback(place, marker)

  # Autocomplete search for google map api
  initAutocompleteWithMap: (map, input, callback) =>
    autocomplete = new (google.maps.places.Autocomplete)(input)
    autocomplete.bindTo 'bounds', map
    infowindow = new (google.maps.InfoWindow)

    marker = new (google.maps.Marker)(
      map: map
      anchorPoint: new (google.maps.Point)(0, -29))

    GoogleMaps.initAutocomplete map, input, (place, marker) ->
      infowindow.close()
      marker.setVisible false

      if !place.geometry
        # User entered the name of a Place that was not suggested and
        # pressed the Enter key, or the Place Details request failed.
        window.alert 'No details available for input: \'' + place.name + '\''
        return
      # If the place has a geometry, then present it on a map.
      if place.geometry.viewport
        map.fitBounds place.geometry.viewport
      else
        map.setCenter place.geometry.location
        map.setZoom 17
        # Why 17? Because it looks good.

      marker.setIcon
        url: place.icon
        size: new (google.maps.Size)(71, 71)
        origin: new (google.maps.Point)(0, 0)
        anchor: new (google.maps.Point)(17, 34)
        scaledSize: new (google.maps.Size)(35, 35)
      marker.setPosition place.geometry.location
      marker.setVisible true

      address = ''

      if place.address_components
        address = [
          place.address_components[0] and place.address_components[0].short_name or ''
          place.address_components[1] and place.address_components[1].short_name or ''
          place.address_components[2] and place.address_components[2].short_name or ''
        ].join(' ')

      infowindow.setContent '<div><strong>' + place.name + '</strong><br>' + address
      infowindow.open map, marker

      if typeof callback == "function"
        callback(place)
      return
    return
