@Profile =
  profileImage:
    previewImageElem: ->
      img = $("img.file-input-image")

      img

    clearPreviewImage: (elem) ->
      img = @previewImageElem()
      parent = $(elem).closest('.image-preview')
      parent.find('.image-preview-filename').val ''
      parent.find('.image-preview-clear').hide()
      parent.find('.image-preview-input input:file').val ''
      parent.find('.image-preview-input input[type="hidden"]').val true
      parent.find('.image-preview-input-title').text 'Browse'
      img.attr 'src', "/assets/profile-icon.png"
      return

    createPreviewImage: (elem, options = {}) ->
      imgElem = @previewImageElem()
      # Create the preview image
      file = elem.files[0]
      reader = new FileReader

      # Set preview image into the popover data-content
      reader.onload = (e) ->
        imgElem.attr 'src', e.target.result

        parent = $(elem).closest('.image-preview')
        parent.find('.image-preview-input input[type="hidden"]').val false
        parent.find('.image-preview-input-title').text 'Change'
        parent.find('.image-preview-clear').show()
        parent.find('.image-preview-filename').val file.name

      reader.readAsDataURL file

  initialize: ->
    # closebtn = $('<button/>',
    #   type: 'button'
    #   text: 'x'
    #   id: 'close-preview'
    #   style: 'font-size: initial;')
    #
    # closebtn.attr 'class', 'close pull-right'
    #
    # $.each $(".image-preview"), ->
    #   content = 'There\'s no image'
    #   if $(this).data("preview-img")
    #     img = previewImageElem()
    #     img.attr 'src', $(this).data("preview-img")
    #     content = img
    #
    #   # Set the popover default content
    #   $(this).popover
    #     trigger: 'hover'
    #     html: true
    #     title: '<strong>Preview</strong>' + $(closebtn)[0].outerHTML
    #     content: content
    #     placement: 'top'

@ProfileWizard =
  initializeWizard: ->
    $('.wizard-tabs > li a[title]').tooltip()
    $(".wizard-tabs > li a[data-step='#{window.location.pathname}']").parent().addClass("active").removeClass("disabled")

  initializeSponsorStep: ->
    $(".business_user_check input[type='radio']").on "change", ->
      $(".business_user_details").toggle()

    $("a.clear-personal-details-fields").on "click", (e) ->
      e.preventDefault()
      $("body").find('form').find("input[type='text'], textarea").val("")
      radioButtons = $("body").find('form fieldset').find("input[type='radio']")
      radioButtons.filter(":checked").prop("checked", false)
      radioButtons.not(":checked").prop("checked", true)

  initializeStudentStep: ->
    todayDate = new Date()
    startDate = new Date(todayDate.getFullYear() - 18, todayDate.getMonth(), 1)
    $('#user_student_attributes_date_of_birth').datepicker({
      format: 'yyyy-mm-dd'
      startDate: startDate
      endDate: todayDate
      todayHighlight: true
    })
