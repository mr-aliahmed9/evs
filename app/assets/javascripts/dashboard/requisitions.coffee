@Offers =
  placeOffer: (elem, requisition_id, event) ->
    console.log requisition_id
    event.preventDefault()
    $this = $(elem)
    notifyType = "info"
    $.ajax
      url: $this.attr("href")
      type: $this.attr("data-method-type")
      dataType: 'json'
      beforeSend: ->
        $this.prop "disabled", true
      success: (result) ->
        if result.status == 200
          notifyType = "info"
          $this.attr("data-method-type", result.method)
          $this.attr("href", result.url)
          totalOfferElem = $("small.total_offers_search_#{result.requisition_id} span")
          totalOffer = parseInt($("small.total_offers_search_#{result.requisition_id} span").text())
          if $this.attr("data-offered") == "false"
            $this.html("Remove Your Offer")
            $this.attr("data-offered", "true")
            $this.removeClass("btn-info").addClass("btn-danger")
            $("ul.sponsors_offers_list_icon_search_#{result.requisition_id}").append result.icon_html
            totalOffer++
            totalOfferElem.text totalOffer
          else
            $this.attr("data-offered", "false")
            $this.html("Place Your Offer")
            $this.removeClass("btn-danger").addClass("btn-info")
            $("ul.sponsors_offers_list_icon_search_#{result.requisition_id}").find("li.sponsor_offer_icon_#{result.offer_id}").remove()
            totalOffer--
            totalOfferElem.text totalOffer
        else
          notifyType = "danger"
        Settings.notifier({message: result.text, type: notifyType})
      error: ->
        Settings.notifier({message: "Something went wrong", type: "danger"})
      complete: ->
        $('[data-toggle="tooltip"]').tooltip()
        $this.prop "disabled", false

@SponsorshipRequisitions =
  mapWindow: ""
  school_ids: []
  validateForm: ->
    that = this
    $('#new_requisition').validate
      rules:
        "requisition[subject]": 'required'
        "requisition[description]": 'required'
        "search_school": 'required'
        "requisition[end_date]": 'required'
        "requisition[start_date]": 'required'
        "requisition[grade_level]": 'required'
      submitHandler: (form) ->
        valid = that.validateSchool(form)
        if !valid
          return false
        else
          form.unbind("submit")
          form.submit()
        return

  validateSchool: (form) ->
    if !$('#requisition_school_id').length
      Settings.notifier({message: "You have not selected any school for this requisition", type: "danger"})
      return false
    else
      return true

  initializeAutoComplete: ->
    that = this
    inputTypeahead = $("[data-provide='typeahead']")

    inputTypeahead.typeahead
      source: (query, process) ->
        inputTypeahead.tooltip("show")

        if $(".removed-school-btn").length
          selected_school = $(".removed-school-btn").data("school-selected")
        else if $("#requisition_school_id").length
          selected_school = true
        else
          selected_school = false

        $.ajax
          url: '/requisitions/sponsorships/list_schools'
          data: query: query, school_selected: selected_school
          dataType: 'json'
          beforeSend: ->
          complete: ->
          success: (result) ->
            result.forEach (value) ->
              that.school_ids.push value.id
            process result
        return
      highlighter: (obj) ->
        item = undefined
        query = undefined
        item = obj
        html = ["<i class='fa fa-graduation-cap'></i>"]

        query = @query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
        name = item.replace(new RegExp('(' + query + ')', 'ig'), ($1, match) ->
          '<strong>' + match + '</strong>'
        )

        html.push name
        html.join(" ")
      updater: (obj) ->
        if $(".remove-requisition-school").length
          $(".remove-requisition-school").attr("data-school-id", obj.id)

        location = obj.address
        map = GoogleMaps.initializedMap
        # Closing all info windows set by marker
        GoogleMaps.markerInfoWindow.forEach (win) ->
          win.close()

        if map and location
          $location = new GoogleMaps
          options =
            zoom: 18
            infoWindow:
              content: that.schoolInfoWindowContent(obj)
          $location.updateMap map, location, options, (window) ->
            that.mapWindow = window
        obj.name
      alignWidth: false

  schoolInfoWindowContent: (school) ->
    actions = ""

    if school.selected == true
      actions += "<p class='text hide-content'>Do you want to add this school in your requisition?</p>"
      actions += "<button type='button' class='btn btn-warning btn-sm removed-school-btn' data-school-selected='true'>You have already selected One School</button> "
      actions += "<button type='button' class='btn btn-danger btn-sm hide-content close-requisition-school-infowindow'>No</button>"
    else
      actions += "<p class='text'>Do you want to add this school in your requisition?</p>"
      actions += "<button type='button' class='btn btn-primary btn-sm add-requisition-school' data-school-selected='false' data-school-id='#{school.id}'>Add</button> "
      actions += "<button type='button' class='btn btn-danger btn-sm close-requisition-school-infowindow'>No</button>"

    infoWindowContent = [
      "<div class='map-school-view map-school-view-#{school.id}'>",
      "<h4 class='map-school-title'>#{school.name}</h4>",
      "<p>#{school.address.city}, #{school.address.state}</p>",
      actions,
      "</div>"
    ].join("")
    return infoWindowContent

  initializeDatePickers: ->
    todayDate = new Date();
    minPediodMonth = todayDate.getMonth() + 3
    startDateForEndDateInput = new Date(new Date().getFullYear(), minPediodMonth, 1)

    maxPeriodMonth = todayDate.getMonth() + 12
    endDate = new Date(new Date().getFullYear(), maxPeriodMonth, 1)

    $('#requisition_start_date').datepicker({
      format: 'MM-yyyy',
      startDate: "0d"
      viewMode: "months",
      minViewMode: "months"
      endDate: startDateForEndDateInput
      todayHighlight: true
    });

    $('#requisition_end_date').datepicker({
      format: 'MM-yyyy',
      startDate: startDateForEndDateInput
      viewMode: "months",
      minViewMode: "months"
      endDate: endDate
      todayHighlight: true
    })

  initializeRequisitions: ->
    @validateForm()
    that = this

    $(document).on "click", ".add-requisition-school", (e) ->
      e.preventDefault()
      $this = $(@)
      $.ajax
        url: "/requisitions/sponsorships/school/#{$this.data("school-id")}.js"
        data: { school_selected: true }
        beforeSend: ->
        success: (result) ->
          Settings.holdForm(false)
          $this.replaceWith "<button type='button' data-school-selected='true' class='btn btn-warning btn-sm removed-school-btn'>School Added</button> "
          $(".map-school-view button.close-requisition-school-infowindow").hide()
          $(".map-school-view p.text").hide()
      return

    $(document).on "click", ".remove-requisition-school", (e) ->
      e.preventDefault()
      $this = $(@)
      that.school_ids.forEach (value) ->
        actions = "<button type='button' class='btn btn-primary btn-sm add-requisition-school' data-school-selected='false' data-school-id='#{value}'>Add</button> "
        $(".map-school-view-#{value} button.removed-school-btn").replaceWith actions

      $(".map-school-view button.close-requisition-school-infowindow").show()
      $(".map-school-view p.text").show()
      $this.closest(".panel").remove()

    $(document).on "click", ".close-requisition-school-infowindow", (e) ->
      e.preventDefault()
      that.mapWindow.close()

@StudentsRequisitions =
  initializeRequisitions: ->
    # Loading Default First Requisitions
    setTimeout(->
      if $(".applied-requisitions").length
        $(".status-section").find("ul li.active a").click()
    , 1200)

    $(".status-section").find("ul li.active a").on "ajax:error", (e, data, status, xhr) ->
      $this = $(".status-section").find("ul li.active a")
      $(".requisition-section").find(".tab-pane.active").removeClass("active")

      tab = $(".requisition-section").find("#" + $this.data("toggle-tab"))
      tab.html("<h3 class='text-center'>There was a problem with the server</h3>");
      tab.addClass("active fade in");

@CommonRequisition =
  initializeRequisitions: ->
    jQuery(".best_in_place").best_in_place()

    # Proceed
    $(".proceed-submission").on "click", (e) ->
      e.preventDefault()
      $this = $(this)
      bootbox.confirm
        title: $this.data "confirm-title"
        message:$this.data "confirm-message"
        buttons:
          confirm:
            label: '<i class="fa fa-check"></i> Yes'
            className: 'btn-success'
          cancel:
            label: '<i class="fa fa-times"></i> No'
            className: 'btn-danger'
        callback: (result) ->
          if result == true
            switch $this.val()
              when "Submit Requisition"
                valid = SponsorshipRequisitions.validateSchool($this.closest("form"));
                if valid
                  $this.unbind("click")
                  $this.click()
              when "Approve Offer"
                if !$("input[name='requisition[provider_id]']:checked").length
                  Settings.notifier({message: "Please select offer in order to proceed", type: "warning"})
                else
                  if $("#sponsor_funds").length and parseFloat($("#sponsor_funds").val()) < 500
                    Settings.notifier({message: "Auditor fee is 500 and You donot have enough funds in your wallet to pay auditor fee", type: "warning"})
                  else
                    $this.unbind("click")
                    $this.click()
            console.log 'This was logged in the callback: ' + result
          return
