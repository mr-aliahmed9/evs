@Reviews =
  openReviewModal: (elem) ->
    that = this
    $.get($(elem).data("href"), (response) ->
      $(".review-modal").html response
    ).done(->
      that.initialize()

      $("#reviewModal").modal({
        show: true
        backdrop: "static"
      })
    )

  submitReview: (form) ->
    modal = $("#reviewModal")
    $form = $(form)
    $.ajax
      url: $form.attr("action")
      type: $form.attr("method")
      data: $form.serialize()
      dataType: "json"
      success: (response) ->
        if response.status == 200
          label = "success"
          modal.modal("hide")
          setTimeout(->
            @location.reload()
          , 1500)
        else
          Settings.holdForm(false)
          form.find("input[type='submit']").val("Submit")
          label = "danger"
        Settings.notifier({message: response.msg, type: label})
      error: (response) ->
        Settings.notifier({message: "There was a problem with the server", type: "danger"})

  initialize: ->
    that = this
    $("#reviewModal").on 'hidden.bs.modal', ->
      $(".review-modal").empty()
      return

    $("#rating-input").rating({
      showClear: false
      showCaption: false
    })

    $("form#reviewForm").validate
      rules:
        "review[comment]": 'required'
      submitHandler: (form) ->
        that.submitReview form
        return false
