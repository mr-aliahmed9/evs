@Search =
  initializeSearch: ->
    $("form#search_requisitions_form").submit (e) ->
      e.preventDefault()
      $this = $(this)
      $renderElem = $(".search-requisitions .applied_requisitions")
      $.ajax
        url: $this.attr("action")
        type: "GET"
        data: $this.serialize()
        beforeSend: ->
          $(".search-requisitions .applied_requisitions").html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> <span style="font-size: 25px;">Loading Results</span>')
        success: (response) ->
          html = $($.parseHTML(response)).find(".search-requisitions .applied_requisitions")
          $(".search-requisitions .applied_requisitions").html(html)
        error: ->
          $(".search-requisitions .applied_requisitions").html("Could not search right now. Contact Tech Support")
          $this.find("input[type='submit']").prop("disabled", false)
        complete: (response) ->
          $('[data-toggle="tooltip"]').tooltip()
          $this.find("input[type='submit']").prop("disabled", false)
  initializeSearchBar: ->
    $(".search-dropdown-btn").click ->
      $(this).parent().toggleClass "open"

    # Start End Dates
    SponsorshipRequisitions.initializeDatePickers()

    $('#start_date').datepicker({
      format: 'MM-yyyy'
      viewMode: "months",
      minViewMode: "months"
      endDate: new Date()
      todayHighlight: true
    })

    $('#end_date').datepicker({
      format: 'MM-yyyy',
      startDate: new Date()
      viewMode: "months",
      minViewMode: "months"
      todayHighlight: true
    })

    # Initializing Autocomplete
    $location = new Location()
    $location.setAutoCompleteSearch()
