class ApplicationController < ActionController::Base
  include Devise::Controllers::CustomHelpers
  include ApplicationHelper

  include ActionView::Helpers::TagHelper
  include ActionView::Context

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_current_location, :unless => :devise_controller?
  before_action :set_params_for_client_side, :make_action_mailer_use_request_host_and_protocol

  with_options unless: :admin_controller? do
    before_action :render_steps!
    before_action :verify_domain
    before_action :render_authenticated_area!, if: :devise_controller?
  end

  helper_method :current_participant

  APPLICATION_USERS.each do |user|
    helper_method "current_#{user}".to_sym

    define_method "current_#{user}" do
      (current_user.send(current_user.active_participant) if current_user and current_user.active_participant == user) || nil
    end

    define_method "authorise_#{user}" do
      if current_user.active_participant == user
        redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard]), flash: { alert: "Sorry! This is not access able for you." }
      end
    end
  end

  layout :layout_by_authentication

  def landing_banner(show = false) @banner = show end;

  def current_participant
    current_user.send(current_user.active_participant)
  end

  def after_sign_in_path_for(resource)
    case resource
      when Admin
        admin_root_url(subdomain: APP_SUBDOMAIN[:admin])
      else
        stored_location_for(:redirect_to) || user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard])
    end
  end

  def set_params_for_client_side
    if request.post? or request.put?
      request.params[:method] = "POST"
      gon.post_params = request.params
    end
  end

  def fetch_requisition_progress_status
    return if session[:requisition_status]
    if [:auditor, :student].include?(current_user.active_participant.to_sym)
      requisitions = current_participant.audit_requisitions

      requisition = if params[:id]
                      requisitions.find_by_id(params[:id])
                    else
                      requisitions.first
                    end

      session[:requisition_status] = requisition.status if requisition
    end
  end

  protected

  def layout_by_authentication
    if dashboard_subdomain? and user_signed_in?
      "dashboard"
    else
      "application"
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope.to_s == 'admin'
      admin_root_path
    else
      root_url(subdomain: APP_SUBDOMAIN[:root_domain])
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :username, :email, :password, :password_confirmation])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :username, :first_name, :last_name, :email])
  end

  # Render profile steps when user signed in newly on site
  def render_steps!
    if user_signed_in? and !devise_logging_out? and dashboard_subdomain?
      if current_user.incomplete_profile?
        flash[:warning] = I18n.t 'profile_steps.incomplete' if current_user.missing_profile_step and current_user.missing_profile_step != "finished"
        redirect_to profiles_build_url((current_user.missing_profile_step || User.profile_steps.first), subdomain: APP_SUBDOMAIN[:dashboard])
      end
    end
  end

  def set_geocode_location!(addresses, options = {})
    gon.location = []
    if addresses.is_a?(Array)
      addresses.each do |address|
        make_address_hash! gon.location, address, options
      end
    else
      make_address_hash! gon.location, addresses, options
    end

    gon.location = "undefined" if gon.location.blank?
  end

  def admin_controller?
    params[:controller].include?("admin")
  end

  def generate_client_token
    if current_sponsor.has_payment_info?
      gon.client_token = Braintree::ClientToken.generate(customer_id: current_sponsor.braintree_customer_id)
    else
      gon.client_token = Braintree::ClientToken.generate
    end
  end

  private

  def make_address_hash!(addresses, address, options = {})
    unless address.nil? or address.attributes.select_keys(Address::GEOCODE_ATTRIBUTES).values.include?(nil)
      location = {
        location: address.location,
        latitude: address.latitude,
        longitude: address.longitude,
        place_id: address.place_id,
        marker: {
          title: address.location
        }
      }

      if options.present?
        location[:marker].merge!(options)
      end

      addresses << location
    end

    addresses
  end
  # saves the location before loading each page so we can return to the
  # right page. If we're on a devise page, we don't want to store that as the
  # place to return to (for example, we don't want to return to the sign in page
  # after signing in), which is what the :unless prevents
  def store_current_location
    url = URI.parse(request.url).tap { |uri| uri.host = concat_with_separator(request.subdomain, uri.host, ".") }.to_s
    store_location_for(:redirect_back, url)
  end

  def make_action_mailer_use_request_host_and_protocol
    ActionMailer::Base.default_url_options[:protocol] = request.protocol
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
end
