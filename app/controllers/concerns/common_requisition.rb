module CommonRequisition
  extend ActiveSupport::Concern

  included do
    before_action only: [:update, :destroy] do |controller|
      if controller.current_participant.is_a?(Student)
        set_requisition_progress
      end
    end
    before_action :set_requisition_progress, only: [:progress]
  end

  private

  # Progress of Students && Auditors
  def set_requisition_progress
    begin
      @requisition =  case current_user.active_participant.to_class
                      when Auditor
                        current_participant.audit_requisitions.find(params[:id])
                      else
                        current_participant.sponsorship_requisitions.find(params[:id])
                      end
    rescue ActiveRecord::RecordNotFound
      redirect_to user_authenticated_root_path, flash: { alert: "Requisition Progress not found" }
    end
  end
end
