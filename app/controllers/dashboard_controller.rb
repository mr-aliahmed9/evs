class DashboardController < ApplicationController
  before_action :render_authenticated_area!

  before_action :generate_client_token, if: Proc.new { |controller| controller.current_user.has_role?(Sponsor) }

  layout "dashboard"

  def index
    action = current_user.active_participant.pluralize
    send(action)
    render action: action.to_sym
  end

  def students
    @sponsorship_requisition = current_student.sponsorship_requisitions
                                        .includes(:school, :audit_offers, :audit_requisition)
                                        .unrejected_requisitions.first

    @rejected_requisitions   = current_student.sponsorship_requisitions.includes(:school, :audit_offers, :audit_requisition => :audit_report).rejected_requisitions
  end

  def sponsors
    @sponsorship_requisitions = current_sponsor.sponsorship_requisitions.includes(:school, :audit_offers, :audit_requisition).order("created_at desc").limit(4)
  end

  def auditors
    @audit_requisition = auditor_on_going_progress
    if @audit_requisition
      set_geocode_location!(@audit_requisition.school.address)
      @audit_report = @audit_requisition.audit_report
    end
    @completed_cases = current_auditor.audit_requisitions.completed_requisitions.includes(:sponsorship_requisition)
  end

  def upload_report
    sleep 6
    Requisition.transaction do
      @audit_requisition = auditor_on_going_progress

      respond_to do |format|
        if @audit_requisition.update_attributes(report_uploaded_at: DateTime.now, status: Requisition::AUDIT_STATUS[:completed])
          @audit_requisition.sponsorship_requisition.generate_results
          flash[:notice] = "Requisition has been successfully completed."
          # NotificationsMailer.report_uploaded(@audit_requisition.sponsorship_requisition.provider.user).deliver
          # NotificationsMailer.results(@audit_requisition.sponsorship_requisition.owner.user).deliver
          format.json { render json: { status: 200, redirect: root_path } }
        else
          format.json { render json: { status: 500 } }
        end
      end
    end
  end

  def auditor_on_going_progress
    current_auditor.audit_requisitions.approved_requisitions.first
  end
end
