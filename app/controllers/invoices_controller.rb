class InvoicesController < ApplicationController
  before_action :set_invoices, only: [:pay_invoices]
  before_action :authorise_student, :authorise_auditor, :authorise_auditor

  def index
    @scope = params[:scope] || "paid"
    @invoices = current_sponsor.invoices.send(@scope == "open" ? "unpaid" : @scope)

    if params[:due].present?
      @due_invoices = true
      @invoices = @invoices.due_current_month_invoices
    elsif params[:due].blank? and params[:due] == false
      @due_invoices = false
      @invoices = @invoices.future_invoices
    end
  end

  def pay_invoices
    failed = @invoices.map(&:pay).uniq.include?(false)
    redirect_to progress_requisition_path(@invoices.first.requisition_id)
    if failed.present?
      flash[:alert] = "There was a problem while Paying Invoice"
    else
      months = []
      @invoices.each do |invoice|
        months << invoice.month_humanize
      end
      flash[:notice] = "Invoices Paid for the month of #{months.join(', ')}"
    end
  end

  def set_invoices
    @invoices = Invoice.where(id: params[:invoice_ids])
  end
end
