class Profiles::BuildController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :render_steps!
  prepend_after_action :set_current_step, only: [:show]
  before_action :set_user, :render_authenticated_area!, :render_dashboard!

  include Wicked::Wizard

  steps(*User.profile_steps) # *unpacks the array and serves as number of arguments for a method

  def show
    case step
    when :personal_details
      unless current_user.roles.present?
        redirect_to profiles_build_path(:who_are_you) and return
      end

      if !current_participant.has_address?
        current_participant.build_address
      else
        set_geocode_location!(current_participant.address, {click_able_marker: true})
      end
    when :payment_info
      if !current_participant.is_a?(Auditor)
        skip_step
      else
        redirect_to profiles_build_url(:profile_picture) and return if !@user.has_info?
      end
    else
      if User.profile_steps.last(3).include?(step) and !@user.has_info?
        redirect_to profiles_build_url(:personal_details) and return
      end
    end
    @user.form_step = step if User.profile_steps.include? step
    render_wizard
  end

  def update
    @user.attributes = user_params(step) unless user_params(step).nil?
    set_geocode_location!(current_participant.address, {click_able_marker: true}) if current_participant and current_participant.address
    case step
    when :who_are_you
      @user.build_role
    end
    @user.participant.form_step = step if @user.participant and User.profile_steps.include? step
    render_wizard @user
  end

  private

  def user_params(step)
    permitted_attributes = case step
                           when :who_are_you
                             [:active_participant]
                           when :personal_details
                             address_attributes = { address_attributes: [ :id, :location, :latitude, :longitude, :place_id, :city, :state, :zip ] }

                             details_attributes = [:first_name, :last_name, :bio_data, :national_id, :username,
                              sponsor_attributes: [
                                :id, :volunteer, :organization, :organization_title
                              ],
                              student_attributes: [:id, :gender, :guardian_monthly_income, :gender, :age, :date_of_birth, :current_school, :guardian_name],
                              auditor_attributes: [:id, :gender, :past_experience, :qualification, :skills]
                             ]

                             details_attributes.each do |attr|
                               if attr.is_a?(Hash)
                                 attr[(current_user.active_participant + "_attributes").to_sym] << address_attributes
                               end
                             end

                             details_attributes
                           when :profile_picture
                             [:avatar, :remove_avatar]
                           when :payment_info
                             [auditor_attributes: [:id, :account_number, :routing_number, :mobile_number]]
                           end
    params.require(:user).permit(permitted_attributes).merge(form_step: step) unless permitted_attributes.nil?
  end

  def finish_wizard_path
    @user.finish_profile!

    flash[:notice] = "Profile Completed"
    allow_popup(true)
    user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard])
  end

  def set_current_step
    @step = step
    session[:profile_current_step] = step #setting step to use globally
  end

  def set_user
    @user = current_user
  end

  # If user access profile steps and profile status is completed then redirect to dashboard
  def render_dashboard!
    if current_user.profile_completed?
      flash[:notice] = "Your profile has been completed"
      redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard])
    end
  end
end
