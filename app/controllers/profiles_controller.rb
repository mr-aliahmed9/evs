class ProfilesController < ApplicationController
  before_action :set_participant

  def show
  end

  def set_participant
    begin
      @participant = User.find(params[:id]).participant
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path, flash: { alert: "Page not found" }
    end
  end
end
