class Requisitions::AuditsController < ApplicationController
  before_action :authorise_auditor, :authorise_student
  before_action :set_requisition, only: [:update]

  APPROVE = "Approve Offer"

  def update
    respond_to do |format|
      if params[:status].present?
        @audit_requisition.status = params[:status].to_i
        @audit_requisition.approved_at = DateTime.now if params[:status].to_i == Requisition::AUDIT_STATUS[:approved]
      end
      if @audit_requisition.update_attributes(audit_requisition_params)
        format.html { redirect_to(progress_requisition_path(@audit_requisition.sponsorship_requisition), :notice => Requisition::APPROVAL_STATE % { providership: "Auditing" }) }
        format.json { respond_with_bip(@audit_requisition) }
      else
        format.json { respond_with_bip(@audit_requisition) }
      end
    end
  end

  private

  def set_requisition
    begin
      @audit_requisition = current_sponsor.sponsorship_requisitions.find(params[:id]).audit_requisition
    rescue ActiveRecord::RecordNotFound
      redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard]), flash: { alert: "Requisition not found" }
    end
  end

  def audit_requisition_params
    params.require(:requisition).permit(:subject, :description, :provider_id, :provider_type)
  end
end
