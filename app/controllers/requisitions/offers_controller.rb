class Requisitions::OffersController < ApplicationController
  before_action do |controller|
    authorise_student
  end

  include OffersHelper

  def create
    @offer = current_participant.offers.build(requisition_id: params[:requisition_id])
    if @offer.save
      # NotificationsMailer.offer_placed(@offer.requisition.owner.user).deliver
      render json: { status: 200, requisition_id: params[:requisition_id], icon_html: sponsors_offers_list_icon_search(@offer).html_safe, offer_id: @offer.id, text: "Offer has been placed", url: requisitions_offer_path(@offer.id), method: :delete }
    else
      heading = content_tag(:ul, :class => 'list-unstyled') do
        @offer.errors[:alert_heading].collect do |item|
          content_tag(:li, item)
        end.join.html_safe
      end
      errors = content_tag(:ul) do
        @offer.errors[:info].collect do |item|
          content_tag(:li, item)
        end.join.html_safe
      end

      render json: { status: false, text: heading + errors }
    end
  end

  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy
    render json: { status: 200, requisition_id: @offer.requisition_id, offer_id: @offer.id, text: "Offer removed", url: requisitions_offers_path(requisition_id: @offer.requisition_id), method: :post }
  end
end
