class Requisitions::ReportsController < ApplicationController
  before_action :authorise_student
  before_action :authorise_sponsor, except: [:show]
  before_action :set_requisition
  before_action :set_audit_items, only: [:new, :create, :edit, :update]
  before_action :set_report, only: [:edit, :update, :show]

  include AuditReportHelper

  def new
    begin
      @report = @requisition.build_audit_report
      @audit_items.each do |item|
        audit_finding = @report.audit_findings.build(audit_item_id: item.id)
        audit_finding.selected_container = AuditFinding::SELECTED_AUDIT_ITEM[:not_selected]
      end

      @audit_findings = @report.audit_findings
    rescue Exception
      redirect_to edit_requisitions_audit_report_path(@requisition, @requisition.audit_report), flash: { alert: "You have already created Audit Report. You can edit/download the report to complete the requisition process" }
    end
  end

  def create
    @report = @requisition.build_audit_report(report_params)
    if @report.save
      redirect_to edit_requisitions_audit_report_path(@requisition, @requisition.audit_report)
    else
      @audit_findings = @report.audit_findings
      render :new
    end
  end

  def edit
    unselected_items = @audit_items.where("name not in (?)", @report.audit_findings.map(&:audit_item).map(&:name))
    selected_items = @report.audit_findings
    selected_items.each do |item|
      item.selected_container = AuditFinding::SELECTED_AUDIT_ITEM[:selected]
    end
    unselected_items.each do |item|
      audit_finding = @report.audit_findings.build(audit_item_id: item.id)
      audit_finding.selected_container = AuditFinding::SELECTED_AUDIT_ITEM[:not_selected]
    end

    @audit_findings = @report.audit_findings.sort_by{|x| @audit_items.map(&:id).index x.audit_item_id}
  end

  def update
    if @report.update_attributes(report_params)
      redirect_to edit_requisitions_audit_report_path(@requisition, @requisition.audit_report)
    else
      @audit_findings = @report.audit_findings.sort_by{|x| @audit_items.map(&:id).index x.audit_item_id}
      render :edit
    end
  end

  def show
    @report.update_attribute(:updated_at, DateTime.now)
    respond_to do |format|
      format.html
      format.pdf do
        pdf  = render_to_string pdf: "EVS-Audit-Report",
               template: 'requisitions/reports/show.pdf.haml',
               layout: '/layouts/evs_audit_report.html.haml',
               print_media_type: true,
               title: "Requisition Audit Report - #{generated_report_format(@report.updated_at)}"
        send_data(pdf, filename: "EVS-Audit-Report-#{generated_report_format(@report.updated_at)}.pdf",  :type=>"application/pdf", :disposition => params[:deposition] || "attachment")
      end
    end
  end

  private

  def report_params
    params.require(:audit_report).permit(:good_controls, :weak_controls, :auditor_id, :audit_findings_attributes => [:id, :observation, :marks, :recommendation, :selected_container, :audit_item_id, :_destroy])
  end

  def set_requisition
    begin
      @requisition = current_participant.audit_requisitions.find(params[:audit_id])
    rescue ActiveRecord::RecordNotFound
      redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard]), flash: { alert: "Requisition not found" }
    end
  end

  def set_report
    @report = @requisition.audit_report
    if !@report
      redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard]), flash: { alert: "Requisition not found" }
    end
  end

  def set_audit_items
    @audit_items = AuditItem.all
  end
end
