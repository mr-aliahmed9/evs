class Requisitions::SponsorshipsController < ApplicationController
  before_action :authorise_sponsor, :authorise_auditor, :except => [:search]
  before_action :set_default_request_format, only: [:schools]
  before_action :authorise_sponsor, :authorise_auditor, only: [:destroy, :update]
  include CommonRequisition

  SAVE    = "Save Details"
  OPEN    = "Submit Requisition"
  APPROVE = "Approve Offer"

  @@selected_school = false

  def new
    @@selected_school = false
    @requisition = current_student.sponsorship_requisitions.build(student_id: current_student.id)
    set_geocode_location!(current_student.address, {click_able_marker: true})
  end

  def create
    @requisition = current_student.sponsorship_requisitions.build(new_requisition_params)
    set_geocode_location!(current_student.address, {click_able_marker: true})
    if @requisition.save
      redirect_to progress_requisition_path(@requisition), notice: "Requisitions has been saved."
    else
      flash[:alert] = "Review Errors"
      render :new
    end
  end

  def list_schools
    @schools = School.all

    if params[:school_selected].present? and params[:school_selected] == "true"
      @@selected_school = true
    else
      @@selected_school = false
    end

    data = @schools.as_json({:only => [:id, :name, :foundation_date], :include => :address, :selected_school => @@selected_school})

    render json: data
  end

  def school
    @school = School.find(params[:school_id])
    @@selected_school = true
    respond_to do |format|
      format.js
    end
  end

  def update
    status = Requisition::SPONSORSHIP_STATUS.fetch_key("draft")

    case params[:commit]
    when OPEN
      @requisition.publish_requisition
      status = Requisition::SPONSORSHIP_STATUS[:open]
      message = Requisition::PUBLIC_STATE % { providership: "Sponsorship" }
    when APPROVE
      @requisition.approved_at = DateTime.now
      status = Requisition::SPONSORSHIP_STATUS[:approved]
      message = Requisition::APPROVAL_STATE % { providership: "Sponsorship" }
    else
      message = Requisition::UPDATE_STATE
    end

    respond_to do |format|
      if @requisition.update_attributes(requisition_params(status))
        format.html { redirect_to(progress_requisition_path(@requisition), :notice => message) }
        format.json { respond_with_bip(@requisition) }
      else
        format.html { render template: "requisitions/progress/student" }
        format.json { respond_with_bip(@requisition) }
      end
    end
  end

  def destroy
    @requisition.destroy
    flash[:notice] = "Your requisition has been removed."
    redirect_to(user_authenticated_root_path)
  end

  private

    def new_requisition_params
      params.require(:requisition).permit(:subject, :description, :grade_level, :school_id, :start_date, :end_date)
    end

    def set_default_request_format
      request.format = :json unless params[:format]
    end

    def requisition_params(status)
      permitted_attributes = [
        :subject, :description, :grade_level, :school_id, :start_date, :end_date
      ]

      if params[:commit] == APPROVE
        permitted_attributes << :provider_type
        permitted_attributes << :provider_id
      end

      params.require(:requisition).permit(permitted_attributes).merge(status: status)
    end
end
