class RequisitionsController < ApplicationController
  before_action :authorise_student, :only => [:search, :show]
  before_action :set_requisition, only: [:show]
  include CommonRequisition

  def progress
    send("#{current_user.active_participant.pluralize}_progress")

    respond_to do |format|
      format.html { render template: "requisitions/progress/#{current_user.active_participant}" }
      format.js
    end
  end

  # Search Public Requisitions (Authorised for Sponsor && Auditor)
  def search
    @requisitions = Requisition.send(current_user.requisition_type).open_requisitions.search(params, current_user)

    if current_user.has_role?(Auditor)
      allow_popup(false)
      allow_popup(true) if current_auditor.has_on_going_case?
    end

    respond_to do |format|
      format.html
      format.json { render json: render_to_string(:partial => "requisitions/shared/search/results", :layout => false) }
    end
  end

  def show
    @sponsorship_requisition = @requisition
    @audit_requisition       = @sponsorship_requisition.audit_requisition

    addresses = [@requisition.student.address, @requisition.school.address]
    set_geocode_location!(addresses, zoom: 18) unless addresses.any?(&:nil?)
  end

  private

  # Public Open Requisitions
  def set_requisition
    begin
      @requisition = Requisition.open_requisitions.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to user_authenticated_root_url(subdomain: APP_SUBDOMAIN[:dashboard]), flash: { alert: "Requisition not found" }
    end
  end

  # Progress of Student's Case
  def students_progress
    @status = @requisition.status
    set_geocode_location!(@requisition.school.address, zoom: 18)

    if Requisition.unapproved_statuses.include? @status.to_sym
      # Since Statuses are not in Approved Category
      # Fetching only requisition for Sponsor
      case @status.to_sym
      when Requisition::SPONSORSHIP_STATUS.fetch_key(:open)
        @sponsorship_offers = @requisition.try(:sponsorship_offers)
      end
    else
      @audit_requisition = @requisition.audit_requisition  # Requisition for Auditor

      case @status.to_sym
      when Requisition::SPONSORSHIP_STATUS.fetch_key(:approved)
        @audit_offers = @audit_requisition.try(:audit_offers)
      when Requisition::SPONSORSHIP_STATUS.fetch_key(:validated)
        # Code Here
      end
    end
  end

  # Progress of Sponsor's Case
  def sponsors_progress
    @sponsorship_requisition = @requisition
    @audit_requisition       = @sponsorship_requisition.audit_requisition
    @audit_offers = @sponsorship_requisition.audit_requisition.audit_offers.includes(:creator)

    addresses = [@requisition.student.address, @requisition.school.address]
    set_geocode_location!(addresses) unless addresses.any?(&:nil?)
  end
end
