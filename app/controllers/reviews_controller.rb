class ReviewsController < ApplicationController
  before_action :set_review, only: [:edit, :update]
  def new
    @review = Review.new(requisition_id: params[:requisition_id], ratee_id: params[:ratee_id], rater_id: params[:rater_id])
    render layout: false
  end

  def edit
    render layout: false
  end

  def create
    @review = Review.new reviews_params
    if @review.save
      render json: { status: 200, msg: "Reviews submission successfully done. Thank You for your support" }
    else
      render json: { status: false, msg: "There was something Wrong" }
    end
  end

  def update
    if @review.update_attributes(reviews_params)
      render json: { status: 200, msg: "Reviews submission successfully done. Thank You for your support" }
    else
      render json: { status: false, msg: "There was something Wrong" }
    end
  end

  private

  def reviews_params
    params.require(:review).permit(:rating, :comment, :requisition_id, :rater_id, :ratee_id)
  end

  def set_review
    @review = Review.find(params[:id])
  end
end
