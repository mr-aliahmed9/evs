class Users::SessionsController < Devise::SessionsController
  skip_before_action :render_authenticated_area!
  def destroy
    current_user.set_missing_profile_step session[:profile_current_step]
    super
  end
end
