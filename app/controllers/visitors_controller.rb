class VisitorsController < ApplicationController
  before_action -> { landing_banner(true) }

  def welcome
    gon.student_count = Student.all.length
    gon.sponsor_count = Sponsor.all.length
    gon.auditor_count = Auditor.all.length
  end
end
