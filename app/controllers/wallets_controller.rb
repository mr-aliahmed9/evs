class WalletsController < ApplicationController
  before_action :set_sponsor

  def create
    result = @sponsor.find_or_make_customer(params)
    if result == true
      @sponsor.funds += params[:wallet_cost].to_f
      @sponsor.save!
      redirect_to dashboard_index_path, notice: "Your funds are saved to your wallet"
    else
      redirect_to dashboard_index_path, flash: { error: "There is something went wrong. Please try again later." }
    end
  end

  private

  def set_sponsor
    @sponsor = current_sponsor
  end
end
