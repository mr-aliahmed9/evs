module ActiveAdmin::SchoolsHelper
  include MapsHelper

  def founded_date(foundation_date)
    years = (Date.today.year - foundation_date.year)
    pluralize(years, "year")
  end
end
