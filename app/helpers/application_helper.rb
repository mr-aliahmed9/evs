module ApplicationHelper
  def number_to_currency(number, options = {})
    options[:locale] ||= I18n.locale
    super(number, options)
  end

  def flash_alert_type(name)
    case name.to_s
    when "notice"
      "success"
    when "warning"
      "warning"
    when "info"
      "info"
    else
      "danger"
    end
  end

  def validation_errors_notifications(object)
    return '' if object.errors.empty?

    messages = object.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
      <div class="alert alert-danger alert-block"> <button type="button"
      class="close" data-dismiss="alert">x</button>
      #{messages}
      </div>
    HTML

    html.html_safe
  end

  def concat_with_separator(value, str, separator)
    value + separator + str
  end

  def allow_popup(allow = true, key = "modal")
    session[key.to_sym] = allow ? true : nil
  end

  def popup_is_set?(key = 'modal')
    session[key.to_sym] == true
  end

  def number_with_decimal(num)
    "%g" % ("%.2f" % num)
  end
end

class ActionView::Helpers::FormBuilder
  def preview_image_field field_name
    help_block = ""
    help_block = '<span id="helpBlock2" class="help-block">Please provide your profile picture</span>' if object.errors[field_name.to_sym].present?
    %Q{
      <div class="form-group #{object.errors[field_name.to_sym].present? ? 'has-error' : ''}">
        <div class="input-group image-preview" data-preview-img="#{object.has_avatar?(field_name) ? object.send(field_name).url(:medium) : ''}">
          <input value="#{object.has_avatar?(field_name) ? object.avatar_file_name : ''}" class="form-control input-sm image-preview-filename" disabled="disabled" type="text"></input>

          <!-- don't give a name === doesn't send on POST/GET -->
          <span class="input-group-btn">
            <!-- image-preview-clear button -->
            <button class="btn btn-default image-preview-clear button" onclick="Profile.profileImage.clearPreviewImage(this);" style="#{!object.has_avatar?(field_name) ? 'display:none;' : ''}" type="button">
              <span class="glyphicon glyphicon-remove"></span>
              Clear
            </button>
            <!-- image-preview-input-->
            <div class="btn btn-default image-preview-input button">
              <span class="glyphicon glyphicon-folder-open"></span>
              <span class="image-preview-input-title">Browse</span>
              <input type='hidden' name='user[remove_avatar]' value="#{object.has_avatar?(field_name) ? false : true}" />
              #{file_field field_name.to_sym, accept: "image/png, image/jpeg, image/gif", onchange: "Profile.profileImage.createPreviewImage(this);"}
            </div>
          </span>
        </div>
        #{ help_block }
      </div>

    }.html_safe
  end
end
