module AuditReportHelper
  def audit_items_icon
    ["fa-question", "fa-file-text-o", "fa-money", "fa-map-marker", "fa-check-square"]
  end

  def result_label
    {
      "passed" => "label-success",
      "failed" => "label-danger"
    }
  end

  def generated_report_format(datetime)
    datetime.strftime("%d %B, %Y - %I:%M %P")
  end
end
