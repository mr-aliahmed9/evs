module DashboardHelper
  def render_progress(progress_owner, options = {}, &block)
    options["#{progress_owner.downcase}_content".to_sym] = block
    render partial: "requisitions/progress/requisition_status", locals: options
  end

  def rating_input(rating)
    %Q{<input class="review-rating rating rating-loading" value="#{rating}" data-display-only='true' data-show-clear="false" data-show-caption="true">}.html_safe
  end

  def rating_status(rating)
    if rating < 2.5
      "label-danger"
    elsif rating == 2.5
      "label-info"
    else
      "label-success"
    end
  end

  # TODO Set Dynamic Content for partials that can help in defining positions based on user type in partials
  # def render_content(content_title)
  #   inner = -> () {
  #     content_title
  #   }
  #   inner.call()
  # end
  #
  # InitiateDynamicPosition = Struct.new (
  #   :partial
  # ) do
  #   def self.set_position(pos)
  #     capture(&)
  #   end
  # end
end
