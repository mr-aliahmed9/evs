module OffersHelper
  def sponsors_offers_list_icon_search(offer)
    icon_html = ActionController::Base.helpers.link_to "javascript:void(0);", data: { toggle: "tooltip", placement: "top", title: offer.creator.user.full_name } do
                  ActionController::Base.helpers.image_tag offer.creator.user.avatar.url(:medium), class: "img-responsive img-circle"
                end
    %Q{
      <li class="col-md-1 sponsor_offer_icon_#{offer.id}">
        #{icon_html}
      </li>
    }
  end
end
