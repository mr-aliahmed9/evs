module Profiles::BuildHelper
  # This helper is created for detection of block inside block
  # so block local variable is passed for detection
  
  # Problem ->
  # The main layout already renders another partial through block
  # and inside of that partial there is another partial which is "Common Details"
  # so based on user role conditions there was a need to check the block_given? method
  # but it always returns true because the common_details partial is already called in a block
  def render_common_details(form, &block)
    render partial: "profiles/build/shared/common_details", locals: { f: form, :block => block }
  end
end
