module RequisitionsHelper
  def short_description(desc)
    truncate(desc, length: 25, omission: '... (continued)')
  end

  def humanize_requisition_status(status)
    status.gsub("_", " ")
  end

  def requisition_progress_icons
    {
      "draft" => "edit-requisitions.png",
      "open" => "open-requisitions.png",
      "approved" => "approved-requisitions.png",
      "validated" => "audit-requisition-completed.png",
      "rejected" => "rejected-requisitions.png",
      "completed" => "completed-requisitions.png"
    }
  end

  def requisition_progress_classes
    {
      "draft" => "primary",
      "open" => "info",
      "approved" => "warning",
      "validated" => "success",
      "rejected" => "danger",
      "completed" => "success"
    }
  end

  def requisition_progress_status(status, requisition_status, participant)
    statuses =  if participant.is_a?(Student)
                  Requisition::SPONSORSHIP_STATUS
                else
                  Requisition::AUDIT_STATUS
                end
    if statuses[status.to_sym] < statuses[requisition_status.to_sym]
      %Q{<s>#{status.capitalize}</s>}
    elsif statuses[status.to_sym] == statuses[requisition_status.to_sym]
      %Q{<i class="fa fa-chevron-left"></i> #{status.capitalize}}
    else
      %Q{#{status.capitalize}}
    end
  end

  def published_format(publish_date)
    publish_date.strftime("%B %d, %Y") rescue nil
  end

  def invoice_status
    {
      "paid" => "success",
      "open" => "primary"
    }
  end
end
