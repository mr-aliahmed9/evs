class ApplicationMailer < ActionMailer::Base
  default from: 'ali.ahmed.cs2016@gmail.com'
  layout 'mailer'
end
