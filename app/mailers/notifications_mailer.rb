class NotificationsMailer < ApplicationMailer
  def waiting_approval(user)
    @user = user
    mail to: @user.email, subject: "Approval Status"
  end

  def offer_placed(user)
    @user = user
    mail to: "ali.ahmed.cs2014@gmail.com", subject: "New Offer Placed"
  end

  def report_uploaded(user)
    @user = user
    mail to: @user.email, subject: "Audit Report Submitted"
  end

  def transaction_completed(user)
    @user = user
    mail to: @user.email, subject: "Transaction Completed"
  end

  def results(user)
    @user = user
    mail to: @user.email, subject: "Report Uploaded"
  end
end
