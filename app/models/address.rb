# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  owner_type :string
#  owner_id   :integer
#  longitude  :decimal(, )
#  latitude   :decimal(, )
#  location   :string
#  place_id   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city       :string
#  state      :string
#  zip        :string
#

class Address < ApplicationRecord
  belongs_to :owner, polymorphic: true
  belongs_to :student, -> { where( addresses: { owner_type: 'Student' } ) }, foreign_key: 'owner_id'

  validates_presence_of :location, :city, :state

  GEOCODE_ATTRIBUTES = ["location", "latitude", "longitude", "place_id"]
end
