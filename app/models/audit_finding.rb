# == Schema Information
#
# Table name: audit_findings
#
#  id              :integer          not null, primary key
#  observation     :string
#  recommendation  :text
#  marks           :float
#  audit_item_id   :integer
#  audit_report_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class AuditFinding < ApplicationRecord
  belongs_to :audit_item
  belongs_to :audit_report

  attr_accessor :selected_container
  cattr_accessor :total_audit_items

  SELECTED_AUDIT_ITEM = { null_selected: 0, selected: 1, not_selected: 2 }

  SELECTED_AUDIT_ITEM.keys.each do |state|
    define_method state.to_s + "?" do
      selected_container.to_s == SELECTED_AUDIT_ITEM[state].to_s
    end
  end

  with_options if: -> { has_selected_item? } do |finding|
    finding.validates :observation, presence: true
    finding.validates :marks, presence: true
    finding.validates :marks, :numericality => { :greater_than => 0, :less_than_or_equal_to => 10 }
  end

  def has_selected_item?
    selected?
  end
end
