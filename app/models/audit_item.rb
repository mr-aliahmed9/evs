# == Schema Information
#
# Table name: audit_items
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AuditItem < ApplicationRecord
  has_many :audit_findings
end
