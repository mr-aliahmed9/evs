# == Schema Information
#
# Table name: audit_reports
#
#  id                   :integer          not null, primary key
#  good_controls        :text
#  weak_controls        :text
#  total_marks          :float
#  result               :string
#  status               :integer          default("incomplete")
#  audit_requisition_id :integer
#  auditor_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class AuditReport < ApplicationRecord
  has_many :audit_findings, dependent: :destroy
  accepts_nested_attributes_for :audit_findings,
    reject_if: :verify_unselected_items

  belongs_to :auditor
  belongs_to :audit_requisition, class_name: "Requisition", foreign_key: :audit_requisition_id

  enum status: { incomplete: 0, completed: 1 }

  TOTAL_REPORT_MARKS = 10
  PASSED = "passed"
  FAILED = "failed"

  validate do
    validate_findings_count
  end

  before_save do
    self.audit_findings = self.audit_findings.to_a.delete_if {|x| x.selected_container.to_i == 2}
    self.set_total_marks
    self.calculate_result
  end

  def validate_findings_count
    if audit_findings.all?(&:null_selected?) || audit_findings.select(&:selected?).length < 2
      errors.add(:base, "Please provide atleast 2 Audit Items for the Report")
    end
  end

  def verify_unselected_items(attributed)
    self.valid? && attributed["observation"].blank? && attributed["recommendation"].blank? && attributed["marks"].blank?
  end

  def set_total_marks
    write_attribute(:total_marks, self.audit_findings.map(&:marks).sum)
  end

  def calculate_result
    self.result =  if percentage > 50
                      PASSED
                    else
                      FAILED
                    end
  end

  def total_report_marks
    TOTAL_REPORT_MARKS * self.audit_findings.length
  end


  def percentage
    # calculating percentage of total marks
    percentage = (self.total_marks * 100) / (total_report_marks)
    percentage
  end
end
