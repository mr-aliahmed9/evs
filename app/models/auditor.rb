# == Schema Information
#
# Table name: auditors
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  personal_note           :text
#  rate                    :string
#  schedule                :string
#  gender                  :integer          default(0)
#  past_experience         :string
#  qualification           :text
#  skills                  :string
#  account_number          :string
#  routing_number          :string
#  mobile_number           :string
#  merchant_account_status :string
#  merchant_account_id     :string
#  funds                   :float
#

class Auditor < ApplicationRecord
  include CommonUser
  include StepValidator

  has_many :transactions, as: :payee

  has_many :audit_requisitions, class_name: "Requisition", as: :provider # Auditor offered requisitions
  has_many :offers, as: :creator, :dependent => :destroy

  with_options if: -> { required_for_step?(:personal_details) } do |step|
    step.validates :gender, presence: true, on: :update
    step.validates :past_experience, presence: true, on: :update
    step.validates :qualification, presence: true, on: :update
    step.validates :skills, presence: true, on: :update
  end

  with_options if: -> { required_for_step?(:payment_info, true) } do |step|
    step.validates :mobile_number, presence: true, on: :update
    step.validates :routing_number, presence: true, on: :update
    step.validates :account_number, presence: true, on: :update
  end

  delegate :received_reviews, to: :user, allow_nil: true

  AUDITOR_FEE = 500

  # For Future Use
  BRAINTREE_MERCHANT_DESTINATION = Braintree::MerchantAccount::FundingDestination::Bank
  BRAINTREE_MASTER_MERCHANT_ID          = "evs"

  def is_a_merchant?
    merchant_account_id.present?
  end

  def has_on_going_case?
    audit_requisitions.approved_requisitions.present?
  end

  def pull_payments_from_wallet(sponsor)
    sponsor_funds = sponsor.funds
    auditor_funds = funds || 0

    if sponsor_funds.present?
      auditor_funds += AUDITOR_FEE
      update_attribute(:funds, auditor_funds)

      sponsor_funds -= AUDITOR_FEE
      sponsor.update_attribute(:funds, sponsor_funds)
    end
  end

  def build_merchant_account
    result = nil
    merchant_account_params = {
      :individual => {
        :first_name => self.user.first_name,
        :last_name => self.user.last_name,
        :email => self.user.name,
        :phone => "5553334444",
        :date_of_birth => "1981-11-19",
        :ssn => "456-45-4567",
        :address => {
          :street_address => "111 Main St",
          :locality => "Chicago",
          :region => "IL",
          :postal_code => "60622"
        }
      },
      :business => {
        :legal_name => "EVS's Ladders",
        :dba_name => "EVS's Ladders",
        :tax_id => "98-7654321",
        :address => {
          :street_address => "111 Main St",
          :locality => "Chicago",
          :region => "IL",
          :postal_code => "60622"
        }
      },
      :funding => {
        :descriptor => "EVS Ladders",
        :destination => BRAINTREE_MERCHANT_DESTINATION,
        :email => self.user.email,
        :mobile_phone => self.mobile_number,
        :account_number => self.account_number,
        :routing_number => self.routing_number
      },
      :tos_accepted => true,
      :master_merchant_account_id => BRAINTREE_MASTER_MERCHANT_ID,
      :id => "#{self.user.username}_auditor_evs"
    }

    unless self.is_a_merchant?
      result = Braintree::MerchantAccount.create(merchant_account_params)
    end

    result
  end
end
