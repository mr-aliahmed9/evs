module CommonUser
  extend ActiveSupport::Concern

  included do
    belongs_to :user
    before_destroy :remove_participant

    has_one :address, as: :owner, dependent: :destroy
    accepts_nested_attributes_for :address
    delegate :full_name, :first_name, :last_name, :username, :email, :total_rating, :requisition_review, to: :user, allow_nil: true
  end

  def has_address?
    address.present?
  end

  def name
    self.class.to_s
  end

  def remove_participant
    if user
      roles = user.roles.reject {|elem| elem == self.name.downcase}
      user.update_attribute(:roles, roles)
    end
  end
end
