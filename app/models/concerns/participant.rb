module Participant
  extend ActiveSupport::Concern

  included do
    serialize :roles, Array

    PARTICIPANTS = APPLICATION_USERS

    PARTICIPANTS.each do |role|
      has_one role.to_sym, dependent: :destroy
      accepts_nested_attributes_for role.to_sym
    end

    after_initialize :default_values
  end

  def fetch_all_participants
    if self.roles
      send_chain(*self.roles).compact
    end
  end

  def build_role
    if active_participant
      transaction do
        update_roles!
        @roles = active_participant.to_class.find_or_create_by(user_id: self.id)
        @roles.save(validate: false)
        @roles
      end
    end
  end

  def participant
    send(self.active_participant)
  end

  def has_role?(value)
    active_participant.to_class == value
  end

  def default_role
    PARTICIPANTS.first
  end

  def set_active_participant(role)
    self.active_participant = role
    self.save(validate: false)
  end

  private

  def default_values
    self.active_participant = default_role if !self.active_participant
    self.profile_status     = User::PROFILE_STATUS[:incomplete_profile] if new_record?
  end

  def update_roles!
    self.roles = (self.roles << active_participant).uniq
    save(validate: false)
  end

  module ClassMethods
    def with_role(role)
      self.where("roles like ?", "%#{role.to_s}%")
    end
  end
end
