module StepValidator
  extend ActiveSupport::Concern

  included do
    attr_accessor :form_step
  end

  def required_for_step?(step, skip = false)
    # All fields are required if no form step is present or No fields no required if skip is true
    return false if skip == true and form_step.nil?
    return true  if skip == false and form_step.nil?

    # All fields from current step are required if the
    # step parameter appears before or we are on the current step
    return true if User.profile_steps.index(step.to_sym) == User.profile_steps.index(form_step.to_sym)
  end

  def valid_with_step?(steps = [])
    steps.include? form_step
  end
end
