# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  issued_at      :datetime
#  requisition_id :integer
#  invoice_amount :float
#  status         :integer          default("open")
#  invoice_number :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  month          :integer
#  year           :integer
#

class Invoice < ApplicationRecord
  belongs_to :requisition
  validates_uniqueness_of :invoice_number

  # default_scope { order("year asc") }

  enum status: { open: 0, paid: 1 }

  scope :unpaid, -> { where("invoices.status = 0") }
  scope :current_month, -> { where("month = ? and year = ?", Date.today.month, Date.today.year) }

  before_create do
    generate_invoice_number
  end

  before_destroy do
    revert_reserved_funds
  end

  def self.generate(requisition)
    ActiveRecord::Base.transaction do
      total = []
      s_date = requisition.start_date
      e_date = requisition.end_date

      (s_date...e_date).select {|d| d.day == 1}.map do |date|
        attrs = {
          issued_at: DateTime.now,
          requisition_id: requisition.id,
          month: date.month,
          year: date.year,
          invoice_amount: requisition.school.fees
        }
        total << attrs[:invoice_amount]
        create(attrs)
      end

      requisition.update_attribute(:reserved_funds, total.sum)
    end
  end

  def self.due_amount
    unpaid.sum(&:invoice_amount)
  end

  def self.total_paid_invoices
    paid.sum(&:invoice_amount)
  end

  def self.total_current_invoices
    current_month.sum(&:invoice_amount)
  end

  def self.due_invoices
    unpaid.select{|i| Date.new(i.year, i.month, 1) <= Date.today}
  end

  def self.future_invoices
    unpaid.select{|i| Date.new(i.year, i.month, 1) > Date.today}
  end

  def self.due_current_month_invoices
    unpaid.select{|i| i.month + 1 == Date.today.month + 1 and i.year == Date.today.year}
  end

  def of_current_month?
    month == Date.today.month and year == Date.today.year
  end

  def pay
    status = false
    begin
      transaction do
        school = requisition.school
        school.update_attribute(:funds, school.funds + invoice_amount)

        sponsor = requisition.provider
        sponsor.update_attribute(:funds, sponsor.funds - invoice_amount)

        requisition.update_attribute(:reserved_funds, requisition.reserved_funds - invoice_amount)

        update_attribute(:status, 1)
        status = true
      end
    rescue => e
      Rails.logger.info "Exception Raised #{e.message}"
      status = false
    end

    status
  end

  def revert_reserved_funds
    if open?
      requisition.update_attribute(:reserved_funds, requisition.reserved_funds - invoice_amount) if requisition.reserved_funds >= invoice_amount
    end
  end

  def generate_invoice_number
    self.invoice_number = "#%04i" % (((self.class.last.try(:invoice_number).split("#").last rescue nil) || 0).to_i + 1).to_s
  end

  def month_humanize
    Date.new(year, month, 1).strftime("%b")
  end
end
