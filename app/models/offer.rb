# == Schema Information
#
# Table name: offers
#
#  id             :integer          not null, primary key
#  creator_type   :string
#  creator_id     :integer
#  requisition_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Offer < ApplicationRecord
  belongs_to :creator, polymorphic: true
  # belongs_to :auditor, -> { where( offers: { creator_type: 'Auditor' } ).includes( :audit_requisitions ) }, foreign_key: 'creator_id'
  # belongs_to :sponsor, -> { where( offers: { creator_type: 'Sponsor' } ).includes( :sponsorship_requisitions ) }, foreign_key: 'creator_id'
  belongs_to :requisition

  # scope :approved, (participant_type) -> {
  #   joins(participant_type.downcase.to_sym => "#{participant_type.requisition_type}_requisitions").where("requisitions.")
  # }

  validate do
    verify_sponsor_wallet
  end

  def verify_sponsor_wallet
    if creator_type == "Sponsor"
      sponsor_funds = creator.funds
      fees = requisition.school.fees
      tenure = requisition.tenure
      tenure_fees = []
      tenure.times.each do |n|
        tenure_fees << fees
      end
      if sponsor_funds < tenure_fees.sum
        errors.add(:alert_heading, "You donot have enough funds into your wallet")
        errors.add(:alert_heading, "Please see below issues:")
        errors.add(:info, "Total Fees for #{tenure} months is #{helper.number_to_currency(tenure_fees.sum)}")
        errors.add(:info, "You wallet must contains the amount of School Fees")
      end
    end
  end
end
