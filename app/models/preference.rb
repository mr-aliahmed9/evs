class Preference
  include Mongoid::Document
  include Mongoid::ActiveRecordBridge

  field :object_id, type: Integer
  field :data, type: Hash

  validates_uniqueness_of :object_id

  belongs_to_record :user, foreign_key: :object_id
end
