# == Schema Information
#
# Table name: requisitions
#
#  id                         :integer          not null, primary key
#  subject                    :string
#  description                :text
#  start_date                 :date
#  end_date                   :date
#  status                     :integer          default("draft")
#  owner_type                 :string
#  owner_id                   :integer
#  provider_type              :string
#  provider_id                :integer
#  completed                  :boolean
#  grade_level                :string
#  school_id                  :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  student_id                 :integer
#  published_at               :datetime
#  sponsorship_requisition_id :integer
#  report_uploaded_at         :datetime
#  approved_at                :datetime
#  reserved_funds             :float
#

class Requisition < ApplicationRecord
  include Publisher
  belongs_to :school
  belongs_to :student

  belongs_to :owner, polymorphic: true
  belongs_to :provider, polymorphic: true

  has_many :sponsorship_offers, -> {
    where("offers.creator_type = 'Sponsor'")
    }, class_name: "Offer", :dependent => :destroy
  has_many :audit_offers, -> {
    where("offers.creator_type = 'Auditor'")
    }, class_name: "Offer", :dependent => :destroy

  has_one :review
  has_many :invoices

  validates_presence_of :subject, :school_id

  # validates_uniqueness_of :owner_id, if: Proc.new { |requisition|
  #   requisition.owner.is_a?(Student) && requisition.owner.sponsorship_requisitions.on_going_case.present?
  # }

  before_create :set_default_values
  after_save :generate_audit_requisition

  has_one   :audit_requisition, class_name: "Requisition", foreign_key: :sponsorship_requisition_id
  belongs_to :sponsorship_requisition, class_name: "Requisition", foreign_key: :sponsorship_requisition_id

  has_one :audit_report, -> { joins(:audit_requisition).where("requisitions.owner_type = 'Sponsor'") }, foreign_key: :audit_requisition_id

  SPONSORSHIP_STATUS = { draft: 0, open: 1, approved: 2, validated: 3, rejected: 5 }
  AUDIT_STATUS       = { open: 1, approved: 2, cancelled: 4, completed: 6 }

  PUBLIC_STATE      = "Requisition has been submited for %{providership} Offers"
  APPROVAL_STATE    = "Requisition has been approved for %{providership}"
  UPDATE_STATE      = "Requisition has been updated successfully"

  (SPONSORSHIP_STATUS.keys | AUDIT_STATUS.keys).each do |status|
    scope "#{status}_requisitions".to_sym, -> { where(status: status) }
  end

  scope :unrejected_requisitions, -> { where("status != ?", SPONSORSHIP_STATUS[:rejected]) }

  scope :sponsorship, -> { where("requisitions.owner_type = 'Student'") }
  scope :audit, -> { where("requisitions.owner_type = 'Sponsor'") }

  scope :on_going_case, -> { where("requisitions.status in (?)", [SPONSORSHIP_STATUS[:approved], SPONSORSHIP_STATUS[:validated]]) }
  scope :completed_case, -> { where("requisitions.status in (?)", [AUDIT_STATUS[:completed], SPONSORSHIP_STATUS[:rejected]]) }

  enum status: {
        :draft     => 0,
        :open      => 1,
        :approved  => 2,
        :validated => 3,
        :cancelled => 4,
        :rejected  => 5,
        :completed => 6
      }

  cattr_accessor :tenure

  # has_attached_file    :report
  # validates_attachment :report, :content_type => { :content_type => %w(application/pdf) }

  def set_default_values
    if self.owner.is_a?(Student)
      self.student_id = self.owner_id
      self.status = SPONSORSHIP_STATUS[:draft]
    end
  end

  def start_date=(value)
    self[:start_date] = Date.parse(value.to_s)
  end

  def end_date=(value)
    self[:end_date] = Date.parse(value.to_s)
  end

  def has_offer?(participant)
    send("#{participant.user.requisition_type}_offers").map(&:creator_id).include?(participant.id)
  end

  def fetch_offer(participant)
    send("#{participant.user.requisition_type}_offers").select{|o| o.creator_id == participant.id }.first
  end

  def publish_requisition
    self.published_at = DateTime.now
  end

  def total_school_fees
    fees = []
    tenure.times.each { fees << school.fees }
    fees.sum
  end

  def generate_audit_requisition
    transaction do
      if self.provider_type == "Sponsor" and self.status.to_sym == :approved and !self.audit_requisition.present?
        sponsor        = self.provider
        requisition_id = self.id
        attr = self.as_json.reject{|key, value| ["id", "owner_type", "owner_id", "provider_id", "provider_type"].include?(key)}
        resq = sponsor.audit_requisitions.build(attr)
        resq.publish_requisition
        resq.sponsorship_requisition_id = requisition_id
        resq.status = AUDIT_STATUS[:open]
        resq.save
      end
    end
  end

  def description_format
    self.description.gsub(/\n/, "<br>").html_safe
  end

  def formatted_start_date
    self.start_date.strftime("%b, %Y")
  end

  def formatted_end_date
    self.end_date.strftime("%b, %Y")
  end

  def tenure
    (self.end_date.month + self.end_date.year * 12) - (self.start_date.month + self.start_date.year * 12)
  end

  def generate_results
    transaction do
      if self.provider_type == "Sponsor" && self.audit_requisition.report_uploaded_at.present?
        audit_requisition = self.audit_requisition
        if audit_requisition.audit_report.result == AuditReport::PASSED
          self.update_attributes(status: Requisition::SPONSORSHIP_STATUS[:validated], report_uploaded_at: DateTime.now)
          Invoice.generate(self)
        else
          self.update_attributes(status: Requisition::SPONSORSHIP_STATUS[:rejected], report_uploaded_at: DateTime.now)
        end
        audit_requisition.provider.pull_payments_from_wallet(self.provider)
      end
    end
  end

  def audit_completed_in
    started = approved_at
    ended   = report_uploaded_at
    fomatter = TimeUnitFormatter.new
    fomatter.format_string = "%d day(s) and %d minute(s)"
    fomatter.format(ended - started) rescue nil
  end

  class << self
    def unapproved_statuses
      [:draft, :open]
    end

    def search(filters, current_participant)
      requisitions = self
      # Including all offers based on Current Participant
      requisitions = requisitions.includes({"#{current_participant.requisition_type}_offers".to_sym => [{creator: :user}]},
                                            :school, :owner => [:address, :user]
                                          ).order("created_at desc")

      filters[:start_date] = nil if filters[:start_date].blank?
      filters[:end_date] = nil   if filters[:end_date].blank?

      filters[:start_date] ||= Date.today.beginning_of_year
      filters[:end_date]   ||= Date.today.end_of_year

      start_date = Date.parse(filters[:start_date].to_s)
      end_date   = Date.parse(filters[:end_date].to_s)

      conditions = []

      if filters[:tenure].present?
        conditions << %q{ tenure_of_requisitions(requisitions.end_date, requisitions.start_date) <= :tenure}
      end

      if start_date == end_date
        conditions << "published_at >= :start_date"
      else
        conditions << "published_at >= :start_date and published_at <= :end_date"
      end

      [:age, :gender].each do |attr|
        if filters[attr].present?
          requisitions = requisitions.joins(:student)
          next if attr == :gender and filters[attr] == "2"
          conditions << "#{attr} = :#{attr}"
        end
      end

      if filters[:grade_level].present?
        conditions << "grade_level like :grade_level"
      end

      [:city, :state, :location].each do |attr|
        if filters[attr].present?
          requisitions = requisitions.joins(student: :address)
          conditions << "#{attr} ilike :#{attr}"
        end
      end

      results = requisitions.where(conditions.join(" AND "), {
        tenure: filters[:tenure], start_date: start_date, end_date: end_date,
        gender: filters[:gender], age: filters[:age], grade_level: "%#{filters[:grade_level]}%",
        city: "%#{filters[:city]}%", state: "%#{filters[:state]}%", location: "%#{filters[:location]}%"
        })

      results
    end
  end
end
