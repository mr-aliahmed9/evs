# == Schema Information
#
# Table name: reviews
#
#  id             :integer          not null, primary key
#  rating         :float            default(2.5)
#  comment        :text
#  requisition_id :integer
#  ratee_id       :integer
#  rater_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Review < ApplicationRecord
  belongs_to :requisition
  belongs_to :ratee, class_name: "User", foreign_key: :ratee_id
  belongs_to :rater, class_name: "User", foreign_key: :rater_id

  validates_presence_of :comment, :rater_id, :ratee_id

  DEFAULT_RATING = 2.5
end
