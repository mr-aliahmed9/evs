# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  name            :string
#  overview        :text
#  address         :string
#  state           :string
#  city            :string
#  staffs          :string
#  students        :string
#  foundation_date :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  fees            :float
#  funds           :float
#

class School < ApplicationRecord
  has_one :address, as: :owner
  accepts_nested_attributes_for :address

  has_many :requisitions, dependent: :destroy
  has_many :transactions, as: :payee

  validates_presence_of :name, :fees

  def as_json(options = {})
    json_to_return = super
    json_to_return[:selected] = options[:selected_school]
    return json_to_return
  end
end
