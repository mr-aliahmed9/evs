# == Schema Information
#
# Table name: sponsors
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  volunteer             :boolean          default(FALSE)
#  organization_title    :string
#  organization          :boolean          default(FALSE)
#  braintree_customer_id :string
#  funds                 :float
#

class Sponsor < ApplicationRecord
  include CommonUser
  include StepValidator
  has_many :audit_requisitions, class_name: "Requisition", as: :owner, :dependent => :destroy # Sponsor created requisitions
  has_many :sponsorship_requisitions, class_name: "Requisition", as: :provider # Sponsor offered requisitions

  has_many :invoices, through: :sponsorship_requisitions

  has_many :reviews, as: :ratee

  delegate :received_reviews, :given_reviews, to: :user

  has_many :offers, as: :creator, :dependent => :destroy

  with_options if: -> { business_user? } do |validate|
    validate.validates_presence_of :organization_title
  end

  def business_user?
    organization == true
  end

  def has_payment_info?
    braintree_customer_id
  end

  def empty_wallet?
    funds <= 0
  end

  def find_or_make_customer(params)
    success = false
    if !has_payment_info?
      result = Braintree::Customer.create(
          :first_name => self.user.first_name,
          :last_name => self.user.last_name,
          :email => self.user.email,
          :payment_method_nonce => params[:payment_method_nonce],
          :credit_card => {
              :options => {
                  :make_default => true
              }
          }
      )
      if result.success?
        success = true
        self.braintree_customer_id = result.customer.id
      end
    else
      success = true
    end

    success
  end

  def funds
    read_attribute(:funds) || 0
  end

  def Hello
    self.includes(:user)
  end
end
