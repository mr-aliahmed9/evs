# == Schema Information
#
# Table name: students
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  age                     :string
#  guardian_monthly_income :string
#  gender                  :integer          default(0)
#  date_of_birth           :date
#  current_school          :string
#  guardian_name           :string
#

class Student < ApplicationRecord
  include CommonUser
  include StepValidator

  has_many :sponsorship_requisitions, class_name: "Requisition", as: :owner, dependent: :destroy # Student created requisitions
  has_many :requisitions, dependent: :destroy

  delegate :given_reviews, to: :user

  with_options if: -> { required_for_step?(:personal_details) } do |step|
    step.validates :age, presence: true, on: :update
    step.validates :date_of_birth, presence: true, on: :update
  end

  # Student Requisition in Validation State
  def audit_requisitions
    resq = requisitions.select{ |r| r.provider_type == "Auditor"}

    unless
      resq = sponsorship_requisitions
    end

    resq
  end
end
