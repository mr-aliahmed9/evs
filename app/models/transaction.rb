# == Schema Information
#
# Table name: transactions
#
#  id                   :integer          not null, primary key
#  bank_account         :string
#  bank_name            :string
#  branch               :string
#  check_number         :string
#  payorder_number      :string
#  amount               :float
#  payee_type           :string
#  payee_id             :integer
#  receipt_file_name    :string
#  receipt_content_type :string
#  receipt_file_size    :integer
#  receipt_updated_at   :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Transaction < ApplicationRecord
  # belongs_to :requisition
  belongs_to :payee, polymorphic: true

  scope :school_transactions, -> { where("payee_type = 'School'") }
  scope :auditor_transactions, -> { where("payee_type = 'Auditor'") }

  validates_presence_of :bank_account, :bank_name, :branch, :check_number, :payorder_number,
                        :payee_type, :payee_id


  before_create do
    set_amount
  end

  def set_amount
    payee = self.payee
    self.amount = payee.funds
    if self.valid?
      payee.funds = 0
      payee.save!
    end

    # NotificationsMailer.transaction_completed(payee.user).deliver
  end
end
