# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  first_name             :string
#  last_name              :string
#  profile_status         :integer          default("incomplete_profile")
#  missing_profile_step   :string
#  roles                  :string
#  active_participant     :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  national_id            :string
#  bio_data               :text
#  username               :string
#  created_from_seed      :boolean          default(FALSE)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  scope :with_profile_status, ->(status) { where(profile_status: PROFILE_STATUS[status]) }

  include Participant
  include StepValidator

  cattr_accessor :profile_steps do
    %i(who_are_you personal_details profile_picture payment_info finished)
  end

  PROFILE_STATUS = { incomplete_profile: 0, profile_completed: 1, blocked: 2 }.freeze

  enum :profile_status => PROFILE_STATUS

  attr_accessor :login, :remove_avatar

  has_attached_file :avatar, styles: {
    medium: "300x300#",
    thumb: "100x100#"
  }, default_url: "/assets/profile-icon.png"

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  # Wizard step based validation
  with_options if: -> { required_for_step?(:personal_details) } do |step|
    # the "required_for_step?" method will not work for inside with_options
    # therefore we need to call this method again in condition
    step.with_options if: -> { role_based_validation and required_for_step?(:personal_details) } do |user|
      user.validates :first_name, presence: true, on: :update
      user.validates :last_name, presence: true, on: :update
      user.validates :national_id, presence: true, on: :update
      user.validates :national_id, format: { with: /^[0-9,\+-]+$/, :multiline => true, on: :update }
      user.validates :username,
        :presence => true,
        :uniqueness => {
          :case_sensitive => false
      }, on: :update
    end
  end

  with_options if: -> { required_for_step?(:profile_picture, true) } do |step|
    step.validates :avatar, presence: true, on: :update
  end

  has_many :received_reviews, class_name: "Review", foreign_key: :ratee_id, dependent: :destroy
  has_many :given_reviews, class_name: "Review", foreign_key: :rater_id, dependent: :destroy

  def active_for_authentication?
    super && !blocked?
  end

  def description_format
    self.description.gsub(/\n/, "<br>").html_safe
  end

  def inactive_message
    if !profile_completed?
      :blocked
    else
      super # Use whatever other message
    end
  end

  def total_rating
    ratings = received_reviews.map(&:rating)
    ratings.sum / ratings.length rescue Review::DEFAULT_RATING
  end

  def requisition_review(requisition)
    received_reviews.where "requisition_id = ?", requisition.id
  end

  def remove_avatar=(value)
    if value == "true"
      self.avatar = nil
    end
  end

  def set_missing_profile_step(step)
    update_attribute(:missing_profile_step, step) if incomplete_profile?
  end

  def has_avatar?(field_name)
    send(field_name).url != send(field_name).options[:default_url]
  end

  def nullify_attributes!(except = nil)
    except ||= non_nullable_attributes
    attribute_names.reject { |attr| except.include?(attr) }.each { |attr| self[attr] = nil }
  end

  def role_based_validation
    (has_role?(Sponsor) and sponsor.try(:organization) == false) || has_role?(Student) || has_role?(Auditor)
  end

  def finish_profile!
    roles = self.roles
    excluded = roles.delete(active_participant)
    update_attributes(profile_status: User::PROFILE_STATUS[:profile_completed], roles: excluded.split)
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  def requisition_type
    return nil if active_participant.to_class == Student
    if active_participant.to_class == Sponsor
      "sponsorship"
    else
      "audit"
    end
  end

  def full_name
    if self.has_role?(Sponsor) and self.participant.try(:organization) == true
      [self.participant.try(:organization_title)].join
    else
      [first_name, last_name].join(" ")
    end
  end

  def has_info?
    info = [self.full_name, self.national_id, self.username, self.participant.try(:address)]
    info.all?(&:present?)
  end

  def picture_from_url(url)
    self.avatar = open(url)
  end

  private

  def non_nullable_attributes
    %w{
      id
      email
      profile_status
      roles
      missing_profile_steps
      active_participant
      encrypted_password
      created_at
      updated_at
    }
  end
end
