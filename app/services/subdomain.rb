module Subdomain
  class Dashboard
    def self.matches? request
      case request.subdomain
        when 'dashboard'
          true
        else
          false
      end
    end
  end
  class Admin
    def self.matches? request
      case request.subdomain
        when 'admin'
          true
        else
          false
      end
    end
  end
end