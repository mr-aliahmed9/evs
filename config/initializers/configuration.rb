# The names of participants in our application
APPLICATION_USERS = %w(sponsor student auditor)

# The hash of subdomains used in our application
APP_SUBDOMAIN = { root_domain: !Rails.env.development? ? "www" : nil, admin: "admin", dashboard: "dashboard" }
