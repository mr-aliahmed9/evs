Rails.application.routes.draw do
  constraints(Subdomain::Admin) do
    devise_for :admins, ActiveAdmin::Devise.config
    ActiveAdmin.routes(self)

    unauthenticated :admin do
      devise_scope :admin do
        root to: "active_admin/devise/sessions#new"
      end
    end

    authenticated :admin do
      root to: "admin/dashboard#index"
    end
  end

  devise_for :users, controllers: { sessions: "users/sessions" }

  constraints(Subdomain::Dashboard) do
    namespace :profiles do
      resources :build
    end

    resources :profiles, only: [:show, :update]

    namespace :requisitions do
      resources :sponsorships, except: [:index] do
        collection do
          get "list_schools"
          get "school/:school_id" => "sponsorships#school"
        end
      end
      resources :audits, except: [:index, :show, :destroy] do
        resources :reports
      end
      resources :offers, only: [:create, :destroy]
    end

    resources :reviews

    resources :invoices, only: [:index] do
      collection do
        put "pay_invoices"
      end
    end

    resources :requisitions, only: [:index, :show] do
      member do
        get "progress"
      end
      collection do
        get "search"
      end
    end


    resources :wallets, only: [:show, :create]

    resources :dashboard, only: [:index] do
      collection do
        put "upload_report" => "dashboard#upload_report", as: :upload_pdf_report
      end
    end
    root 'dashboard#index', as: :user_authenticated_root
  end


  root to: 'visitors#welcome'
end
