class AddProfileStatusToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :profile_status, :integer, default: 0
  end
end
