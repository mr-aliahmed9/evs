class AddMissingProfileStepToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :missing_profile_step, :string
  end
end
