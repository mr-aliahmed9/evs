class AddFieldsToSponsors < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :volunteer, :boolean
    add_column :sponsors, :overview, :text
    add_column :sponsors, :organization_title, :string
  end
end
