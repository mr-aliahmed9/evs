class AddFieldsToAuditors < ActiveRecord::Migration[5.0]
  def change
    add_column :auditors, :overview, :text
    add_column :auditors, :personal_note, :text
    add_column :auditors, :rate, :string
    add_column :auditors, :schedule, :string
  end
end
