class AddActiveRoleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :active_role, :string
    rename_column :users, :role, :roles
  end
end
