class AddDefaultFalseVolunteerToSponsors < ActiveRecord::Migration[5.0]
  def change
    change_column :sponsors, :volunteer, :boolean, :default => false
  end
end
