class AddOrganizationToSponsors < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :organization, :boolean, default: false
  end
end
