class AddBraintreeCustomerIdAndFundsToSponsors < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :braintree_customer_id, :string
    add_column :sponsors, :funds, :string
  end
end
