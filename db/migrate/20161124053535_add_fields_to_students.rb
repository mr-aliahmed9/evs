class AddFieldsToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :age, :string
    add_column :students, :guardian_monthly_income, :string
    add_column :students, :overview, :string
  end
end
