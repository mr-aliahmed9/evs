class CreateRequisitionsOld < ActiveRecord::Migration[5.0]
  def change
    create_table :requisitions do |t|
      t.string :subject
      t.text :description
      t.integer :student_id

      t.timestamps
    end
  end
end
