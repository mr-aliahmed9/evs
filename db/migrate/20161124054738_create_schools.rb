class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.string :name
      t.text :overview
      t.string :address
      t.string :state
      t.string :city
      t.string :staffs
      t.string :students
      t.date :foundation_date

      t.timestamps
    end
  end
end
