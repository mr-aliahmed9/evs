class CreateRequisitionsSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :requisitions_schools do |t|
      t.integer :requisition_id
      t.integer :school_id

      t.timestamps
    end
  end
end
