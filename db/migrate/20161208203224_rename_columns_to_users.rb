class RenameColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :active_role, :active_participant
  end
end
