class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :owner_type
      t.integer :owner_id
      t.decimal :longitude
      t.decimal :latitude
      t.string :location
      t.string :place_id

      t.timestamps
    end
  end
end
