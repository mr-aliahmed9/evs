class DropRequisitions < ActiveRecord::Migration[5.0]
  def change
    drop_table :requisitions
    drop_table :requisitions_schools
  end
end
