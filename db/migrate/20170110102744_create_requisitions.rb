class CreateRequisitions < ActiveRecord::Migration[5.0]
  def change
    create_table :requisitions do |t|
      t.string :subject
      t.text :description
      t.date :start_date
      t.date :end_date
      t.integer :status
      t.string :owner_type
      t.integer :owner_id
      t.string :provider_type
      t.integer :provider_id
      t.boolean :completed
      t.string :grade_level
      t.references :school, foreign_key: true

      t.timestamps
    end
  end
end
