class AddFeesToSchools < ActiveRecord::Migration[5.0]
  def change
    add_column :schools, :fees, :float
  end
end
