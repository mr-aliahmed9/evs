class AddAttachmentReportToRequisitions < ActiveRecord::Migration
  def self.up
    change_table :requisitions do |t|
      t.attachment :report
    end
  end

  def self.down
    remove_attachment :requisitions, :report
  end
end
