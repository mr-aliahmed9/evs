class AddStudentIdToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :student_id, :integer
  end
end
