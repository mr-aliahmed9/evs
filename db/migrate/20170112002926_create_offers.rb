class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.string :creator_type
      t.integer :creator_id
      t.integer :requisition_id

      t.timestamps
    end
  end
end
