class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :ntn_number, :string
    add_column :users, :bio_data, :text
  end
end
