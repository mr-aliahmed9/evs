class RemoveColumnsFromRoles < ActiveRecord::Migration[5.0]
  def self.up
    remove_column :students, :overview
    remove_column :sponsors, :overview
    remove_column :auditors, :overview
  end

  def self.down
    add_column :students, :overview, :string
    add_column :sponsors, :overview, :text
    add_column :auditors, :overview, :text
  end
end
