class AddColumnsToRoles < ActiveRecord::Migration[5.0]
  def change
    add_column :auditors, :gender, :integer, default: 0
    add_column :auditors, :past_experience, :string
    add_column :auditors, :qualification, :text
    add_column :auditors, :skills, :string

    add_column :students, :gender, :integer, default: 0
    add_column :students, :date_of_birth, :date
    add_column :students, :current_school, :string
    add_column :students, :guardian_name, :string
  end
end
