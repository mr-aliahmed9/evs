class AddPaymentDetailsToAuditors < ActiveRecord::Migration[5.0]
  def change
    add_column :auditors, :account_number, :string
    add_column :auditors, :routing_number, :string
    add_column :auditors, :mobile_number, :string
    add_column :auditors, :merchant_account_status, :string
    add_column :auditors, :merchant_account_id, :string
  end
end
