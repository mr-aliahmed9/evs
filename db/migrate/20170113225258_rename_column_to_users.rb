class RenameColumnToUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :ntn_number, :national_id
  end
end
