class AddPublishedAtToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :published_at, :datetime
  end
end
