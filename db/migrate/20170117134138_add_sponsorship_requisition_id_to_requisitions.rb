class AddSponsorshipRequisitionIdToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :sponsorship_requisition_id, :integer
  end
end
