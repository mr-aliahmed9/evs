class CreateAuditFindings < ActiveRecord::Migration[5.0]
  def change
    create_table :audit_findings do |t|
      t.string :observation
      t.text :recommendation
      t.float :marks
      t.integer :audit_item_id
      t.integer :audit_report_id

      t.timestamps
    end
  end
end
