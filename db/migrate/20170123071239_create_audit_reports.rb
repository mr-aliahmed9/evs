class CreateAuditReports < ActiveRecord::Migration[5.0]
  def change
    create_table :audit_reports do |t|
      t.text :good_controls
      t.text :weak_controls
      t.float :total_marks
      t.string :result
      t.integer :status, default: false
      t.integer :audit_requisition_id
      t.integer :auditor_id

      t.timestamps
    end
  end
end
