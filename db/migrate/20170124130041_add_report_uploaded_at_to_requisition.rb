class AddReportUploadedAtToRequisition < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :report_uploaded_at, :datetime
  end
end
