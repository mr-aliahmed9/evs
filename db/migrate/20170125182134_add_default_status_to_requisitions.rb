class AddDefaultStatusToRequisitions < ActiveRecord::Migration[5.0]
  def self.up
    change_column :requisitions, :status, :integer, :default => 0
  end

  def self.down
    change_column :requisitions, :status, :integer, :default => nil
  end
end
