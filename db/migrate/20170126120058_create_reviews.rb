class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.float :rating, default: 2.5
      t.text :comment
      t.integer :requisition_id
      t.integer :ratee_id
      t.integer :rater_id

      t.timestamps
    end
  end
end
