class AddApprovedAtToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :approved_at, :datetime
  end
end
