class AddRequisitionTenureCalculationFunction < ActiveRecord::Migration[5.0]
  def self.up
    execute %q{
      CREATE OR REPLACE FUNCTION tenure_of_requisitions(end_date date, start_date date)
      RETURNS integer
      AS
      $$
      DECLARE
        result_record integer;

      BEGIN
      	select
      		(EXTRACT(month from end_date) + (EXTRACT(year from end_date) * 12)) -
      		(EXTRACT(month from start_date) + (EXTRACT(year from start_date) * 12)) into result_record
      	;

      	RETURN result_record;

      END
      $$ LANGUAGE plpgsql;
    }
  end

  def self.down
    execute "drop function tenure_of_requisitions(date, date) cascade"
  end
end
