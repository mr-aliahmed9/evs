class ChangeFundsTypeSponsors < ActiveRecord::Migration[5.0]
  def self.up
    change_column :sponsors, :funds, 'float USING CAST(funds AS float)'
  end
  def self.down
    change_column :sponsors, :funds, :string
  end
end
