class AddFundsToAuditorsAndSchools < ActiveRecord::Migration[5.0]
  def change
    add_column :auditors, :funds, :float
    add_column :schools, :funds, :float
  end
end
