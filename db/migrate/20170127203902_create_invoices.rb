class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.datetime :issued_at
      t.integer :requisition_id
      t.float :invoice_amount
      t.integer :status
      t.string :invoice_number

      t.timestamps
    end
  end
end
