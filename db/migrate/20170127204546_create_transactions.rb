class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.string :bank_account
      t.string :bank_name
      t.string :branch
      t.string :check_number
      t.string :payorder_number
      t.float :amount
      t.string :payer_type
      t.integer :payer_id
      t.attachment :receipt

      t.timestamps
    end
  end
end
