class AddCreatedFromSeedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :created_from_seed, :boolean, default: false
  end
end
