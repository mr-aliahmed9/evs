class AddAuditReportIdToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :audit_report_id, :integer
  end
end
