class RemoveAuditReportIdAndReportFromRequisitions < ActiveRecord::Migration[5.0]
  def change
    remove_column :requisitions, :audit_report_id
    remove_attachment :requisitions, :report
  end
end
