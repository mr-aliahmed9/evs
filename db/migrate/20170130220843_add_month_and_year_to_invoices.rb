class AddMonthAndYearToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :month, :integer
    add_column :invoices, :year, :integer
  end
end
