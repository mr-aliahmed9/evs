class AddDefaultStatusToInvoice < ActiveRecord::Migration[5.0]
  def self.up
    change_column :invoices, :status, :integer, :default => 0
  end

  def self.down
    change_column :invoices, :status, :integer, :default => nil
  end
end
