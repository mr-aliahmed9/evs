class AddReservedFundsToRequisitions < ActiveRecord::Migration[5.0]
  def change
    add_column :requisitions, :reserved_funds, :float
  end
end
