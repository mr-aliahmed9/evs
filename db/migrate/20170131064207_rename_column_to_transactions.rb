class RenameColumnToTransactions < ActiveRecord::Migration[5.0]
  def change
    rename_column :transactions, :payer_id, :payee_id
    rename_column :transactions, :payer_type, :payee_type
  end
end
