# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170131064207) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "addresses", force: :cascade do |t|
    t.string   "owner_type"
    t.integer  "owner_id"
    t.decimal  "longitude"
    t.decimal  "latitude"
    t.string   "location"
    t.string   "place_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "city"
    t.string   "state"
    t.string   "zip"
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "audit_findings", force: :cascade do |t|
    t.string   "observation"
    t.text     "recommendation"
    t.float    "marks"
    t.integer  "audit_item_id"
    t.integer  "audit_report_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "audit_items", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audit_reports", force: :cascade do |t|
    t.text     "good_controls"
    t.text     "weak_controls"
    t.float    "total_marks"
    t.string   "result"
    t.integer  "status",               default: 0
    t.integer  "audit_requisition_id"
    t.integer  "auditor_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "auditors", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "personal_note"
    t.string   "rate"
    t.string   "schedule"
    t.integer  "gender",                  default: 0
    t.string   "past_experience"
    t.text     "qualification"
    t.string   "skills"
    t.string   "account_number"
    t.string   "routing_number"
    t.string   "mobile_number"
    t.string   "merchant_account_status"
    t.string   "merchant_account_id"
    t.float    "funds"
    t.index ["user_id"], name: "index_auditors_on_user_id", using: :btree
  end

  create_table "invoices", force: :cascade do |t|
    t.datetime "issued_at"
    t.integer  "requisition_id"
    t.float    "invoice_amount"
    t.integer  "status",         default: 0
    t.string   "invoice_number"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "month"
    t.integer  "year"
  end

  create_table "offers", force: :cascade do |t|
    t.string   "creator_type"
    t.integer  "creator_id"
    t.integer  "requisition_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "requisitions", force: :cascade do |t|
    t.string   "subject"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "status",                     default: 0
    t.string   "owner_type"
    t.integer  "owner_id"
    t.string   "provider_type"
    t.integer  "provider_id"
    t.boolean  "completed"
    t.string   "grade_level"
    t.integer  "school_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "student_id"
    t.datetime "published_at"
    t.integer  "sponsorship_requisition_id"
    t.datetime "report_uploaded_at"
    t.datetime "approved_at"
    t.float    "reserved_funds"
    t.index ["school_id"], name: "index_requisitions_on_school_id", using: :btree
  end

  create_table "reviews", force: :cascade do |t|
    t.float    "rating",         default: 2.5
    t.text     "comment"
    t.integer  "requisition_id"
    t.integer  "ratee_id"
    t.integer  "rater_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "schools", force: :cascade do |t|
    t.string   "name"
    t.text     "overview"
    t.string   "address"
    t.string   "state"
    t.string   "city"
    t.string   "staffs"
    t.string   "students"
    t.date     "foundation_date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.float    "fees"
    t.float    "funds"
  end

  create_table "sponsors", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "volunteer",             default: false
    t.string   "organization_title"
    t.boolean  "organization",          default: false
    t.string   "braintree_customer_id"
    t.float    "funds"
    t.index ["user_id"], name: "index_sponsors_on_user_id", using: :btree
  end

  create_table "students", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "age"
    t.string   "guardian_monthly_income"
    t.integer  "gender",                  default: 0
    t.date     "date_of_birth"
    t.string   "current_school"
    t.string   "guardian_name"
    t.index ["user_id"], name: "index_students_on_user_id", using: :btree
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "bank_account"
    t.string   "bank_name"
    t.string   "branch"
    t.string   "check_number"
    t.string   "payorder_number"
    t.float    "amount"
    t.string   "payee_type"
    t.integer  "payee_id"
    t.string   "receipt_file_name"
    t.string   "receipt_content_type"
    t.integer  "receipt_file_size"
    t.datetime "receipt_updated_at"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "profile_status",         default: 0
    t.string   "missing_profile_step"
    t.string   "roles"
    t.string   "active_participant"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "national_id"
    t.text     "bio_data"
    t.string   "username"
    t.boolean  "created_from_seed",      default: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  add_foreign_key "auditors", "users"
  add_foreign_key "requisitions", "schools"
  add_foreign_key "sponsors", "users"
  add_foreign_key "students", "users"
end
