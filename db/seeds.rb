user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email

# Preference.find_or_create_by(object_id: 0) do |preference|
#   preference.data = {"name" => "Education Voucher Scehme", "location" => { "latitude" => 24.981417, "longitude" => 67.082491}}
# end
# puts 'System Preference Created'

# School Addresses
@school_addresses = [
              { location: 'Asra Public School', city: "Karachi", state: 'Sindh', latitude: 24.9793289, longitude: 67.06512440000006, place_id: 'ChIJq6qq6uRAsz4ReWtwSkTBR4k' },
              { location: 'Asra Public School Campus 2, Shahrah-e-Usman', city: 'Karachi', state: 'Sindh', latitude: 24.9728025, longitude: 67.06492419999995, place_id: 'ChIJ11QC_etAsz4RxzWmQSCF-DU' },
              { location: 'Falconhouse Grammar School Campus 1', city: 'Karachi', state: 'Sindh', latitude: 24.9292376, longitude: 67.03963399999998, place_id: 'ChIJ3UZCTps_sz4R7nhp2aazbTw' },
              { location: 'Falconhouse Grammar School Campus 5', city: 'Karachi', state: 'Sindh', latitude: 24.9305966, longitude: 67.0390966, place_id: 'ChIJrYg3WJw_sz4RrVek6jR6yxs' },
              { location: "Generation's School", city: 'Karachi', state: 'Sindh', latitude: 24.9024188, longitude: 67.00548509999999, place_id: 'ChIJLWLKmJw_sz4R8f_J1bS7S38' },
              { location: 'Programmer School', city: 'Karachi', state: 'Sindh', latitude: 24.9723701, longitude: 67.06205090000003, place_id: 'ChIJf23OJ-xAsz4RiaiFQYwZXIM' },
              { location: 'The Academy', city: 'Karachi', state: 'Sindh', latitude: 24.9171007, longitude: 67.08157740000001, place_id: 'ChIJ4dPufDo_sz4RuTmZNH_4t3Q' },
              { location: 'The Academy Gulshan Campus', city: 'Karachi', state: 'Sindh', latitude: 24.9279757, longitude: 67.1034631, place_id: 'ChIJU3aPMcg4sz4RUWwwQjUbOnU' }
            ]

@school_addresses.each do |location|
  School.transaction do
    school = School.find_or_create_by(name: location[:location])
    school.staffs = rand(999)
    school.students = rand(9999)
    school.fees = rand(9999)
    school.overview = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    school.foundation_date = (10..20).to_a.sample.years.ago
    school.save!
    puts school.name + ' Saved'
    if school.new_record?
      begin
        address = school.build_address(place_id: location[:place_id])
        address.city = location[:city]
        address.state = location[:state]
        address.latitude = location[:latitude]
        address.longitude = location[:longitude]
        address.location = [location[:location], location[:city], 'Pakistan'].join(", ")
        address.save
        puts school.name + ' Address Created'
      rescue Exception => e
        puts "Address Exception (Reverting) - #{e}"
      end
    end
  end
end

puts 'Creating Audit Items'
# Audit Items
@audit_items = [
                'Physical Interview',
                'Guardian Documnts Verification',
                'Financial Strength',
                'Distance Verification',
                'Guardian Availability Verification'
              ]

@audit_items.each do |item|
  puts 'Creating ' << item
  AuditItem.find_or_create_by(name: item)
end

@data_through_seed = User.where('created_from_seed = true')

unless @data_through_seed.present?
  puts '=============Destroying All Users=============='
  User.destroy_all
  default_pwd = 'click123'
  avatars = [
      'https://cdn2.iconfinder.com/data/icons/flat-design-icons-set-2/256/face_human_blank_user_avatar_mannequin_dummy-512.png'
  ]
  @participants = []
  10.times.each { |n| @participants << 'sponsor' }
  30.times.each { |n| @participants << 'student' }
  10.times.each { |n| @participants << 'auditor' }
  # Total Users

  puts '=============Creating Default Users=============='
  ActiveRecord::Base.transaction do
    @participants.each do |participant|
      user = User.find_or_create_by(username: Faker::Internet.unique.user_name) do |u|
        u.first_name            = Faker::Name.first_name
        u.last_name             = Faker::Name.last_name
        u.email                 = Faker::Internet.email
        u.password              = default_pwd
        u.password_confirmation = default_pwd
        u.picture_from_url avatars.sample
        u.bio_data              = Faker::Lorem.paragraph(4, false, 8)
        u.national_id           = Faker::Address.unique.postcode
        u.active_participant    = participant
        u.build_role
        u.created_from_seed     = true # A check not to create users and further data through seed after once created
        u.finish_profile!
      end

      puts 'User #' + user.id.to_s + ' Created'

      address           = user.participant.build_address
      address.location  = Faker::Address.street_name
      address.city      = Faker::Address.city
      address.state     = Faker::Address.state
      address.latitude  = Faker::Address.latitude
      address.longitude = Faker::Address.longitude
      address.place_id  = ""
      address.save

      puts 'User #' + user.id.to_s + ' Address Created'
    end
  end

  puts "=============Creating Participant's Details=============="
  ActiveRecord::Base.transaction do
    User.where('active_participant in (?)', ['student', 'auditor']).each do |user|
      participant = user.participant
      participant.gender = rand(2)
      case participant.class.to_s
      when 'Student'
        participant.age                     = rand(18).to_s
        participant.guardian_monthly_income = rand(15000).to_s
        participant.date_of_birth           = Faker::Date.between(18.years.ago, Date.today)
        participant.current_school          = 'No'
        participant.guardian_name           = Faker::Name.name
      else
        participant.personal_note           = Faker::Lorem.sentence(10, true)
        participant.past_experience         = rand(7).to_s + 'years'
        participant.qualification           = Faker::Lorem.paragraph
        participant.skills                  = Faker::Lorem.sentences(1, true).join(", ")
      end

      participant.save

      puts 'Participants Information of Participant#' + participant.id.to_s + ' Created'
    end
  end

  puts '=============Creating Students Requisitions=============='
  ActiveRecord::Base.transaction do
    Student.all.each do |student|
      student.sponsorship_requisitions.build do |resq|
        maxDate = Time.now.to_date
        minDate = (Time.now + 1.year).to_date
        # months = (maxDate.month + maxDate.year * 12) - (minDate.month + minDate.year * 12)

        resq.subject       = Faker::Lorem.sentences(2, true).join(" ")
        resq.description   = Faker::Lorem.paragraphs.join
        resq.school_id     = School.all.map(&:id).sample
        resq.grade_level   = (1..12).map{|m| m}.sample
        resq.start_date    = rand(maxDate..(minDate - 3.months)).to_datetime.beginning_of_month
        end_date           = rand(resq.start_date.to_date..minDate)
        if end_date.month >= resq.start_date.to_date.month
          end_date = minDate
        end

        resq.end_date      = end_date.to_datetime.beginning_of_month
        resq.publish_requisition
        resq.save!
        resq.update_attribute(:status, Requisition::SPONSORSHIP_STATUS[:open])
        puts "Student Requisition # #{resq.id.to_s} Created"
      end
    end

    Requisition.audit.destroy_all
    Requisition.all.update_all(status: Requisition::SPONSORSHIP_STATUS[:open], provider_type: nil, provider_id: nil, approved_at: nil)
    approved_sponsors = []
    Sponsor.limit(5).each do |sp|
      Requisition.sponsorship.each do |r|
        next if approved_sponsors.include?(sp.id)
        sponsorship_requisition = Requisition.find(r.id)
        sponsorship_requisition.provider_type = 'Sponsor'
        sponsorship_requisition.provider_id   = sp.id
        sponsorship_requisition.status        = Requisition::SPONSORSHIP_STATUS[:approved]
        sponsorship_requisition.approved_at   = DateTime.now
        sponsorship_requisition.save!

        approved_sponsors << sp.id
      end
    end

    puts Requisition.where("provider_type = 'Sponsor'").count

    approved_auditors = []
    Auditor.limit(5).each do |a|
      Requisition.audit.each do |r|
        next if approved_auditors.include?(a.id)
        audit_requisition = Requisition.find(r)
        audit_requisition.provider_type = 'Auditor'
        audit_requisition.provider_id   = a.id
        audit_requisition.status        = Requisition::AUDIT_STATUS[:approved]
        audit_requisition.approved_at   = DateTime.now
        audit_requisition.save!

        approved_auditors << a.id
      end
    end
    puts Requisition.where("provider_type = 'Auditor'").count
  end

  File.open('db/sample/users.txt', 'w') do |file_line|
    %w(Student Sponsor Auditor).each do |r|
      file_line.puts 'ID || UserName || Email || FirstName || LastName'
      r.constantize.all.each do |role|
        file_line.puts "#{role.id} || #{role.username} || #{role.email} || #{role.first_name} || #{role.last_name}"
      end
    end
  end
end
