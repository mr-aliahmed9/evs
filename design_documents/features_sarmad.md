# Possible Features List prepared by Sarmad Sabih

## Social Education Hub
* Students can express their desires about the books they want to read, about the courses they want to get enrolled in. Other people can list the books they have and are willing to giveaway to a needy person who has expressed a desire to read that book. Needy person will be notified that a book in your wish-list is available as a giveaway and hook both the guys up so that the giveaway can be made. Giver (person who gave the book away) can be given goodwill points as recognition on the platform and a motivation to do more and inspire others to do the same. Likewise, if a person (probably a giver) has some course material which a needy person has express their desire about, then the needy person can be notified (so he can get in the touch with giver and get the course material).

* Givers and sponsors can chat real time with the Students (needy people) to evaluate their interest, seriousness, commitment and aptitude before deciding whether to sponsor their studies (could be whole studies, major or minor courses or things like that which can help them get their career started). This will allow the sponsors to be satisfied before putting their money on someone's education.

* Get schools & educational institutions involved by signing up with us and offer a certain number of needy students discounts on their fees. Again, this can be a motivational factor for others to compete in goodwill.

* Give exposure to the people planning to, or already setup free or inexpensive education. Expose them to donors, sponsors and people willing to help in any capacity.

* Put those schools or educational institutions with free or inexpensive education on city's map with details so the people can look them up easily.
