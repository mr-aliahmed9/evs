module Devise
  module Controllers
    # These helpers are custom addition to devise helpers
    # Devise other modules can be included her
    module CustomHelpers
      # when user hits the Log Out Button
      def devise_logging_out?
        devise_controller? && request.delete?
      end

      # restrict user to access page when subdomain is present and not signed in
      # and redirect to root url or sign in page if user is on devise controller
      def verify_domain
        if dashboard_subdomain? and !user_signed_in?
          authenticate_user! || (redirect_to root_url(subdomain: APP_SUBDOMAIN[:root_domain]))
        end
      end

      # set current url with subdomain if not include and user is signed in
      # Note: we have to skip the method on those controller on which we don't the behavior
      # Example: users/sessions controller
      def render_authenticated_area!
        if !dashboard_subdomain? and user_signed_in?
          redirect_to subdomain: APP_SUBDOMAIN[:dashboard], :controller => params[:controller], :action => params[:action]
        end
      end

      def dashboard_subdomain?
        request.subdomain == APP_SUBDOMAIN[:dashboard]
      end
    end

    module Helpers
    #   Overwrite devise methods here
    end
  end
end
