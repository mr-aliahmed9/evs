class Object
  def send_chain(*args)
    o = self

    args.map do |arg|
      case arg
      when Array
        o.send(arg.first, *arg.last)
      when String, Symbol
        o.send(arg)
      else
        raise ArgumentError
      end
    end
  end
end

class String
  def to_class
    self.capitalize.constantize
  end
end

class Array
  def find_by_class_name(class_name)
    self.select { |elem| elem.to_class == class_name }
  end
end

class Hash
  def fetch_key(value)
    self.key(self[value.to_sym])
  end

  def select_keys(args)
    select {|k,v| args.include?(k) }
  end
end
