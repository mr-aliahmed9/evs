# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  owner_type :string
#  owner_id   :integer
#  longitude  :decimal(, )
#  latitude   :decimal(, )
#  location   :string
#  place_id   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  city       :string
#  state      :string
#  zip        :string
#

FactoryGirl.define do
  factory :address do
    owner_type "MyString"
    owner_id 1
    longtitude "9.99"
    latitude "9.99"
    location "MyString"
    place_id "MyString"
  end
end
