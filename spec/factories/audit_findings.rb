# == Schema Information
#
# Table name: audit_findings
#
#  id              :integer          not null, primary key
#  observation     :string
#  recommendation  :text
#  marks           :float
#  audit_item_id   :integer
#  audit_report_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :audit_finding do
    observation "MyString"
    recommendation "MyText"
    marks 1.5
    audit_item_id 1
    audit_report_id 1
  end
end
