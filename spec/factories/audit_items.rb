# == Schema Information
#
# Table name: audit_items
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :audit_item do
    name "MyString"
  end
end
