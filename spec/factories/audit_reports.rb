# == Schema Information
#
# Table name: audit_reports
#
#  id                   :integer          not null, primary key
#  good_controls        :text
#  weak_controls        :text
#  total_marks          :float
#  result               :string
#  status               :integer          default("incomplete")
#  audit_requisition_id :integer
#  auditor_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :audit_report do
    good_controls "MyText"
    weak_controls "MyText"
    total_marks 1.5
    result "MyString"
    status 1
    audit_requisition_id 1
    auditor_id 1
  end
end
