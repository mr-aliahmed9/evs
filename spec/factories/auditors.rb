# == Schema Information
#
# Table name: auditors
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  personal_note           :text
#  rate                    :string
#  schedule                :string
#  gender                  :integer          default(0)
#  past_experience         :string
#  qualification           :text
#  skills                  :string
#  account_number          :string
#  routing_number          :string
#  mobile_number           :string
#  merchant_account_status :string
#  merchant_account_id     :string
#  funds                   :float
#

FactoryGirl.define do
  factory :auditor do
    user nil
  end
end
