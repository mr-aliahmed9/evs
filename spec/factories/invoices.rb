# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  issued_at      :datetime
#  requisition_id :integer
#  invoice_amount :float
#  status         :integer          default("open")
#  invoice_number :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  month          :integer
#  year           :integer
#

FactoryGirl.define do
  factory :invoice do
    issued_at "2017-01-28 01:39:03"
    requisition_id 1
    invoice_amount 1.5
    status 1
    invoice_number "MyString"
  end
end
