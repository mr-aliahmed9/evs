# == Schema Information
#
# Table name: offers
#
#  id             :integer          not null, primary key
#  creator_type   :string
#  creator_id     :integer
#  requisition_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :offer do
    creator_type "MyString"
    creator_id 1
    requisition_id 1
  end
end
