# == Schema Information
#
# Table name: requisitions
#
#  id                         :integer          not null, primary key
#  subject                    :string
#  description                :text
#  start_date                 :date
#  end_date                   :date
#  status                     :integer          default("draft")
#  owner_type                 :string
#  owner_id                   :integer
#  provider_type              :string
#  provider_id                :integer
#  completed                  :boolean
#  grade_level                :string
#  school_id                  :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  student_id                 :integer
#  published_at               :datetime
#  sponsorship_requisition_id :integer
#  report_uploaded_at         :datetime
#  approved_at                :datetime
#  reserved_funds             :float
#

FactoryGirl.define do
  factory :requisition do
    subject "MyString"
    description "MyText"
    start_date "2017-01-10"
    end_date "2017-01-10"
    status 1
    owner_type "MyString"
    owner_id 1
    provider_type "MyString"
    provider_id 1
    completed false
    grade_level "MyString"
    school nil
  end
end
