# == Schema Information
#
# Table name: requisitions_schools
#
#  id             :integer          not null, primary key
#  requisition_id :integer
#  school_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :requisitions_school do
    requisition_id 1
    school_id 1
  end
end
