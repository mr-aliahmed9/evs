# == Schema Information
#
# Table name: reviews
#
#  id             :integer          not null, primary key
#  rating         :float            default(2.5)
#  comment        :text
#  requisition_id :integer
#  ratee_id       :integer
#  rater_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :review do
    rating 1.5
    comment "MyText"
    requisition_id 1
    auditor_id 1
  end
end
