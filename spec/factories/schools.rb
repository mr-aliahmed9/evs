# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  name            :string
#  overview        :text
#  address         :string
#  state           :string
#  city            :string
#  staffs          :string
#  students        :string
#  foundation_date :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  fees            :float
#  funds           :float
#

FactoryGirl.define do
  factory :school do
    name "MyString"
    overview "MyText"
    address "MyString"
    state "MyString"
    city "MyString"
    staffs "MyString"
    students "MyString"
    foundation_date "2016-11-24"
  end
end
