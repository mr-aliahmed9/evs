# == Schema Information
#
# Table name: sponsors
#
#  id                    :integer          not null, primary key
#  user_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  volunteer             :boolean          default(FALSE)
#  organization_title    :string
#  organization          :boolean          default(FALSE)
#  braintree_customer_id :string
#  funds                 :float
#

FactoryGirl.define do
  factory :sponsor do
    user nil
  end
end
