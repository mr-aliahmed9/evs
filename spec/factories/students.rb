# == Schema Information
#
# Table name: students
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  age                     :string
#  guardian_monthly_income :string
#  gender                  :integer          default(0)
#  date_of_birth           :date
#  current_school          :string
#  guardian_name           :string
#

FactoryGirl.define do
  factory :student do
    user nil
  end
end
