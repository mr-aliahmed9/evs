# == Schema Information
#
# Table name: transactions_invoices
#
#  id             :integer          not null, primary key
#  transaction_id :integer
#  invoice_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :transactions_invoice do
    transaction_id 1
    invoice_id 1
  end
end
