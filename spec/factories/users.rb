# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  first_name             :string
#  last_name              :string
#  profile_status         :integer          default("incomplete_profile")
#  missing_profile_step   :string
#  roles                  :string
#  active_participant     :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  national_id            :string
#  bio_data               :text
#  username               :string
#  created_from_seed      :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :user do
    first_name "Test"
    last_name "User"
    home_address "Abc Street"
    email "test@example.com"
    password "please123"
    profile_status 1
    active_participant User::PARTICIPANTS.sample

    trait :non_validated do
      first_name nil
      home_address nil
      profile_status 0
    end

    trait :incomplete do
      profile_status 0
    end
  end
end
