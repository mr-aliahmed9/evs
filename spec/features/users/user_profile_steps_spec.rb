# Feature: Profile Steps
#   As a user
#   I want to sign in
#   So I can complete my profile steps
feature 'Profile Steps', :devise, :js do
  before(:each) do
    @role = User::PARTICIPANTS.sample
    @user = FactoryGirl.create(:user, profile_status: 0)
  end

  # 1 - Scenario (a): User sign's in and redirect to first step
  #   Given I do not exist as a user
  #   When I sign in with valid credentials
  #   Then I am redirected to first step of wizard
  scenario "user logs in and redirects to first step" do
    signin(@user.email, @user.password)
    expect(page).to have_content "Tell us Who Are You"
  end

  # 1 - Scenario (b): User get register and redirect to first step
  #   Given I do not exist as a user
  #   When I register to the site
  #   Then I am redirected to first step of wizard
  # scenario "user registers and redirects to first step" do
  #   sign_up_with('test_1@example.com', 'please123', 'please123')
  #   expect(page).to have_content "Tell us Who Are You"
  # end

  # 2 - Scenario (a): User sign's in and redirect to last page if log out during wizard
  #   Given I do not exist as a user
  #   When I sign in with valid credentials
  #   And I log out without finishing profile steps
  #   And I sign in again with valid credentials
  #   Then I am redirected to last step of wizard with message
  scenario "user redirects to last step" do
    signin(@user.email, @user.password)
    signout
    signin(@user.email, @user.password)
    expect(page).to have_content I18n.t 'profile_steps.incomplete'
  end

  # 2 - Scenario (b): User sign's in and redirect to last page if after clicking Next
  scenario "user redirect to last step even user move to next step" do
    signin(@user.email, @user.password)
    click_button "Next"
    signout
    signin(@user.email, @user.password)
    expect(page).to have_content I18n.t 'profile_steps.incomplete'
  end

  # 3 - Scenario (a): User selects role
  # Given I see 3 roles
  # When I check randomly to any radio button
  # Then I see my selected role name on next page
  scenario "user selects any role first time" do
    signin(@user.email, @user.password)
    check_role_button(@role)
    click_button "Next"
    expect(page).to have_content @role.capitalize
  end

  # 3 - Scenario (b): User sees the selected role checked when click previous button
  # Given I return back
  # Then I see my selected role checked
  scenario "user selected role displays as checked" do
    signin(@user.email, @user.password)
    check_role_button(@role)
    click_button "Next"
    click_link "Previous"

    # Examples
    # page.should have_xpath("//input[@id='user_assigned_role_#{@role}']")
    # expect(find(:xpath, "//label[@for='user_assigned_role_#{@role}']")).to be_nil
    # expect(page).to have_selector("label[for='user_assigned_role_#{@role}']", text: "Sponsor")
    # expect(page.find("label[for='user_assigned_role_#{@role}']").text.to_s).to eq @role.capitalize.to_s

    # In order to execute jQuery we have to make the browser to wait for at least 3 seconds
    sleep 3
    # As we are using bootstrap awesome checkboxes which hides the native radio element and add it's own css in
    # ::after and ::before css of label
    # The browser couldn't find the native element to pass the test
    # Remove the custom class and bring the native element on front
    page.execute_script('$("label[for=\'user_assigned_role_'""+ @role +""'\']").closest("div").removeClass("checkbox")')

    expect(page.find(:xpath, "//*[@id='user_assigned_role_#{@role}']")).to be_checked
  end

  # 3 - Scenario: User cannot move to next step if validation fails
  # Given I am on personal detail step
  # When I click next button
  # Then I see missing fields
  # scenario "user cannote move to next step if validation fails" do
  #   @user = FactoryGirl.create(:user, :non_validated)
  #   check_role_button(@role)
  #   click_button "Next"
  #
  #   # Step 2
  #   click_button "Next"
  #   expect(page).to have_css "div.user_first_name", text: "First Namecan't be blank"
  # end
end
