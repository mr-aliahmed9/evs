# == Schema Information
#
# Table name: audit_reports
#
#  id                   :integer          not null, primary key
#  good_controls        :text
#  weak_controls        :text
#  total_marks          :float
#  result               :string
#  status               :integer          default("incomplete")
#  audit_requisition_id :integer
#  auditor_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe AuditReport, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
