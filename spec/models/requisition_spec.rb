# == Schema Information
#
# Table name: requisitions
#
#  id                         :integer          not null, primary key
#  subject                    :string
#  description                :text
#  start_date                 :date
#  end_date                   :date
#  status                     :integer          default("draft")
#  owner_type                 :string
#  owner_id                   :integer
#  provider_type              :string
#  provider_id                :integer
#  completed                  :boolean
#  grade_level                :string
#  school_id                  :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  student_id                 :integer
#  published_at               :datetime
#  sponsorship_requisition_id :integer
#  report_uploaded_at         :datetime
#  approved_at                :datetime
#  reserved_funds             :float
#

require 'rails_helper'

RSpec.describe Requisition, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
