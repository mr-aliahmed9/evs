# == Schema Information
#
# Table name: schools
#
#  id              :integer          not null, primary key
#  name            :string
#  overview        :text
#  address         :string
#  state           :string
#  city            :string
#  staffs          :string
#  students        :string
#  foundation_date :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  fees            :float
#  funds           :float
#

require 'rails_helper'

RSpec.describe School, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
