# == Schema Information
#
# Table name: transactions
#
#  id                   :integer          not null, primary key
#  bank_account         :string
#  bank_name            :string
#  branch               :string
#  check_number         :string
#  payorder_number      :string
#  amount               :float
#  payee_type           :string
#  payee_id             :integer
#  receipt_file_name    :string
#  receipt_content_type :string
#  receipt_file_size    :integer
#  receipt_updated_at   :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe Transaction, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
