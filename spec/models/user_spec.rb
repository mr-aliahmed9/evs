# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  first_name             :string
#  last_name              :string
#  profile_status         :integer          default("incomplete_profile")
#  missing_profile_step   :string
#  roles                  :string
#  active_participant     :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  national_id            :string
#  bio_data               :text
#  username               :string
#  created_from_seed      :boolean          default(FALSE)
#

describe User, type: :model do

  before(:each) do
    @user = FactoryGirl.create(:user, email: "user@example.com")
  end

  subject { @user }

  it { should respond_to(:email) }

  it "#email returns a string" do
    expect(@user.email).to match 'user@example.com'
  end

  it "#active_participant returns a set role" do
    expect(@user.active_participant).not_to be_nil
  end

  describe "#build_role" do
    it "returns user's #active_participant role" do
      @user.build_role
      active = @user.roles.select{|r| r == @user.active_participant}
      expect(@user.active_participant).to eq(active.join)
    end

    it "returns if user #has_role?" do
      @user.build_role
      expect(@user.has_role?(@user.active_participant.to_class)).to be(true)
    end

    it "returns nil if user role not passed" do
      @user.active_participant = nil
      @user.build_role
      expect(@user.roles).to be_empty
    end

    it "returns user if user role is created" do
      role = @user.build_role
      expect(role.user.id).to eq(@user.id)
    end

    it "verify user can have multiple roles" do
      first_r = @user.active_participant

      @user.build_role

      # this is to check if user active role is not same as first
      loop do
        @user.active_participant = User::PARTICIPANTS.sample
      end if first_r != @user.active_participant

      second_r = @user.active_participant

      # Second Time Generating another association as per #active_participant attribute is getting set in loop
      @user.build_role

      roles = @user.roles - [first_r, second_r] # eliminating same items if matched

      # Result will be of 0 items in user.roles arr
      expect(roles.length).to eq 0
    end

    it "#fetch_all_participants returns users correct role association" do
      generate_multiple_roles(@user, User::PARTICIPANTS)

      expect(@user.fetch_all_participants.count).to eq(User::PARTICIPANTS.length)

      present_role = false
      @user.fetch_all_participants.each do |generated_r|
        present_role = true if User::PARTICIPANTS.include?(generated_r.name.downcase)
      end

      expect(present_role).to be true
    end

    it "#role.destroy updates user roles and pop (take out) that role from user roles array" do
      role = @user.build_role
      role.destroy
      expect(role.user.roles).to be_empty
    end
  end

  describe "User relations are destroyed as well when user is destroyed" do
    # before(:each) do
    #   @user.build_role
    # end

    it "destroys users roles" do
      generate_multiple_roles(@user, User::PARTICIPANTS)
      @user.destroy
      expect(@user.fetch_all_participants.count).to eq(0)
    end
  end
end
