Capybara.register_driver :chrome do |app|
  # optional
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.timeout = 120

  Capybara::Selenium::Driver.new(app, :browser => :chrome, :http_client => client)
end

Capybara.javascript_driver = :chrome
Capybara.default_max_wait_time = 2


# Setting Default Ports

DEFAULT_HOST = Rails.application.config.action_controller.default_url_options[:host]
DEFAULT_PORT = Rails.application.config.action_controller.default_url_options[:port]

RSpec.configure do |config|
  Capybara.default_host = "http://#{DEFAULT_HOST}"
  Capybara.server_port = DEFAULT_PORT
  Capybara.app_host = "http://#{DEFAULT_HOST}:#{Capybara.server_port}"
end
