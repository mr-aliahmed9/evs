RSpec.configure do |config|

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    config.use_transactional_fixtures = false
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, :js => true) do
    config.use_transactional_fixtures = false
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.append_after(:each) do
    DatabaseCleaner.clean
  end
end
