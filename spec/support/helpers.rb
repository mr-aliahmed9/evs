require 'support/helpers/session_helpers'
require 'support/helpers/devise_request_spec_helpers'
require 'support/helpers/profiles_steps/steps_helpers'
require 'support/helpers/model_helpers'
RSpec.configure do |config|
  config.include Features::SessionHelpers, type: :feature
  config.include Features::DeviseRequestSpecHelpers, type: :feature
  config.include Features::ProfileSteps::StepsHelpers, type: :feature
  config.include Model::Helpers, type: :model
end
