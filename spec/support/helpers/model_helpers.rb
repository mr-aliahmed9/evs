module Model
  module Helpers
    def generate_multiple_roles(user, arr = [])
      arr.each do |r|
        user.active_participant = r
        user.build_role
      end
    end
  end
end