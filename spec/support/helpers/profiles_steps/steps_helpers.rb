module Features
  module ProfileSteps
    module StepsHelpers
      def check_role_button(role)
        find(:xpath, "//label[@for='user_assigned_role_#{role}']").click
      end
    end
  end
end